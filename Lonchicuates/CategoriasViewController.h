//
//  CategoriasViewController.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-08.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriasViewController : UIViewController<UIScrollViewDelegate>{

    UINavigationBar *bar;
    UIButton *botonMenu;
    UIScrollView *scrollView;
    NSTimer *timer;
    UILabel *navTittle;
    
    NSString *imageName;
    
    UIImageView *displayImage;
    UIImage *CatImage;
    NSArray *lonchiAtletas;
    NSArray *lonchiCuates;
    int lonchiCOunt;
}

@property (nonatomic, retain)  UIScrollView *scrollView;
@property (nonatomic, retain)  NSArray *lonchiAtletas;
@property (nonatomic, retain)  NSArray *lonchiCuates;

@property(strong, nonatomic) IBOutlet UINavigationBar *bar;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
@property (nonatomic, retain)  NSTimer *timer;
  @property (nonatomic, retain)  UIImageView *displayImage;

@property (nonatomic, retain) NSString *imageName;

 @property (nonatomic, retain) IBOutlet    UILabel *navTittle;
 @property (nonatomic, retain) UIImage *CatImage;
@property (nonatomic, strong) NSString *categoria;


-(IBAction)click;
-(IBAction)click2;
@end
