//
//  MenuViewController.m
//  Lonchicuates
//
//  Created by Alan MB on 12/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "MenuViewController.h"
#import "ECSlidingViewController.h"
#import "CategoriasViewController.h"
#import "WEBmasViewController.h"
#import "MasViewController.h"
#import "TodasRecetasViewController.h"
#import "Ingrediente-RecetaView.h"

@interface MenuViewController ()

@property (strong, nonatomic) NSArray *menu;
@property (strong, nonatomic) NSArray *section1;
@property (strong, nonatomic) NSArray *section2;
@property (strong, nonatomic) NSArray *section3;
@property (strong, nonatomic) NSArray *section4;

@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;

@end

@implementation MenuViewController
@synthesize menu,section1,section2,section3,section4, recetas, menuSearch, searchBar, searchDisplayController, searchArray, menuTable, searchTable, titulosRecetas, ingredienteAC, ingrediente;


- (id)initWithStyle:(UITableViewStyle)style
{
  //  self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    searchCount = 0;
    ///
    [super viewDidLoad];
    NSArray *ingArray = [[NSArray alloc]initWithObjects:@"Aceite de olivo", @"Aceite en aerosol", @"Agua de mango", @"Agua simple", @"Aguacate", @"Ajo", @"Almendras", @"Apio", @"Arándanos", @"Aros de manzana", @"Arroz cocido", @"Arroz blanco", @"Azúcar", @"Bolillo", @"Brócoli", @"Cacahuates", @"Cacahuates enchilados", @"Camarones", @"Canela", @"Canela en polvo", @"Cebolla", @"Claras de huevo", @"Cereal de trigo", @"Champiñones", @"Chile piquín", @"Chips de maíz", @"Chocolate", @"Coctel de frutas", @"Crema", @"Crema de cacahuate", @"Cuadritos de queso", @"Espinaca", @"Esquites", @"Fresas", @"Frijoles molidos", @"Frijoles refritos", @"Frambuesas", @"Fusilli", @"Garbanzos", @"Galletas de animalitos", @"Galletas de chocolate o vainilla", @"Galletas habaneras", @"Galletas marías", @"Galletas saladas integrales", @"Gelatina", @"Granola", @"Hot cakes", @"Huevo", @"Jamaica", @"Jamón de pavo", @"Jitomate", @"Jitomate cherry", @"Jicama", @"Jocoque", @"Jugo de naranja", @"Kiwi", @"Leche", @"Lechuga", @"Lechuga romana", @"Limón", @"Mandarínas", @"Mango", @"Mantequilla", @"Manzana verde o roja", @"Mayonesa", @"Melón", @"Melón verde", @"Mermelada de fresa", @"Mermelada de zarzamora", @"Miel de abeja", @"Miel de maple", @"Mostaza", @"Moras", @"Naranja", @"Nueces", @"Palitos de pan", @"Palitos de pepino", @"Papaya", @"Palillos de papel o plástico", @"Plátano", @"Pan integral", @"Pan molido", @"Papa", @"Pasta de chocolate (nutella)", @"Pasta de jitomate", @"Pechuga de pollo", @"Pechuga de pavo", @"Pepino", @"Pera", @"Pimienta", @"Piña", @"Piña en almibar", @"Pretzels", @"Puré de tomate", @"Queso cheddar", @"Queso crema", @"Queso Oaxaca", @"Queso manchego", @"Queso mozzarella", @"Queso panela", @"Queso philadelphia", @"Requesón", @"Sal", @"Sal de ajo", @"Salsa de soya", @"Sazonador en polvo sabor mantequilla", @"Sopes", @"Surimi", @"Toronja", @"Tortillas de harina", @"Tostadas horneadas", @"Toalla de papel", @"Uva", @"Yoghurt natural", @"Zanahorias baby", @"Zarzamoras", @"Té helado", @"Sandía", @"Nopales", @"Palomitas", @"Zanahoria rallada", @"Agua de limón", @"Rollitos de jamón", @"Uvas con ate", @"Rebanada de jamón", @"Agua de naranja", @"Fresas con yoghurt", @"Manzana", @"Zanahoria", @"Vinagre balsámico", @"Tostadas", @"Bolsita de té de frutas", @"Tortillas de maíz", nil];
    self.ingredienteAC =ingArray;
    
    NSArray *ingArray2 = [[NSArray alloc]initWithObjects:@"Aceite de olivo", @"Aceite en aerosol", @"Agua de mango", @"Agua simple", @"Aguacate", @"Ajo", @"Almendras", @"Apio", @"Arandanos", @"Aros de manzana", @"Arroz cocido", @"Arroz blanco", @"Azucar", @"Bolillo", @"Brocoli", @"Cacahuates", @"Cacahuates enchilados", @"Camarones", @"Canela", @"Canela en polvo", @"Cebolla", @"Claras de huevo", @"Cereal de trigo", @"Champiñones", @"Chile piquin", @"Chips de maiz", @"Chocolate", @"Coctel de frutas", @"Crema", @"Crema de cacahuate", @"Cuadritos de queso", @"Espinaca", @"Esquites", @"Fresas", @"Frijoles molidos", @"Frijoles refritos", @"Frambuesas", @"Fusilli", @"Garbanzos", @"Galletas de animalitos", @"Galletas de chocolate o vainilla", @"Galletas habaneras", @"Galletas marias", @"Galletas saladas integrales", @"Gelatina", @"Granola", @"Hot cakes", @"Huevo", @"Jamaica", @"Jamon de pavo", @"Jitomate", @"Jitomate cherry", @"Jícama", @"Jocoque", @"Jugo de naranja", @"Kiwi", @"Leche", @"Lechuga", @"Lechuga romana", @"Limon", @"Mandarinas", @"Mango", @"Mantequilla", @"Manzana verde o roja", @"Mayonesa", @"Melon", @"Melon verde", @"Mermelada de fresa", @"Mermelada de zarzamora", @"Miel de abeja", @"Miel de maple", @"Mostaza", @"Moras", @"Naranja", @"Nueces", @"Palitos de pan", @"Palitos de pepino", @"Papaya", @"Palillos de papel o plastico", @"Platano", @"Pan integral", @"Pan molido", @"Papa", @"Pasta de chocolate (nutella)", @"Pasta de jitomate", @"Pechuga de pollo", @"Pechuga de pavo", @"Pepino", @"Pera", @"Pimienta", @"Piña", @"Piña en almibar", @"Pretzels", @"Pure de tomate", @"Queso cheddar", @"Queso crema", @"Queso oaxaca", @"Queso manchego", @"Queso mozzarella", @"Queso panela", @"Queso philadelphia", @"Requeson", @"Sal", @"Sal de ajo", @"Salsa de soya", @"Sazonador en polvo sabor mantequilla", @"Sopes", @"Surimi", @"Toronja", @"Tortillas de harina", @"Tostadas horneadas", @"Toalla de papel", @"Uva", @"Yoghurt natural", @"Zanahorias baby", @"Zarzamoras", @"Te helado", @"Sandia", @"Nopales", @"Palomitas", @"Zanahoria rallada", @"Agua de limon", @"Rollitos de jamon", @"Uvas con ate", @"Rebanada de jamon", @"Agua de naranja", @"Fresas con yoghurt", @"Manzana", @"Zanahoria", @"Vinagre balsamico", @"Tostadas", @"Bolsita de te de frutas", @"Tortillas de maiz", nil];
    self.ingrediente = ingArray2;
    
    
    NSArray *array = [[NSArray alloc]initWithObjects:  @"La vitamina que c necesita", @"A mover el esqueleto", @"Dale a tu cuerpo más energía", @"Ánimo para campeonas y campeones", @"Tus huaraches con hierro", @"Ahí viene la A", @"Para que desquites, tus esquites", @"Fibra para la panza", @"Como los japonesitos", @"Para aplicarse, un burrito", @"Don Pepino va a la escuela", @"Ponte fresa", @"Las espinacas se llevan con las fresas", @"¿Me das una mordidta?", @"La defensiva contra las infecciones", @"Bueno para los nervios", @"¡Mmmh! Que rico mi mollete", @"Para crecer en grande", @"Mamma mía ¡Que italiano!", @"Tún, tún, sushi con atún", @"¿Qué hongo mi champiñón?", @"¡Muy mexicanos y muy nutritivos!", @"Sana sana, la manzana", @"De todo un poco", @"Te presentamos a la Tía Mina", @"Trenecito verde", @"Lancitas africanas", @"Solecito verde", @"Frutiflechas", @"Caracoches", @"Espiroapios", @"Las prietas están tostaditas", @"Papa ratón", @"El tricolor", @"El francesito volador", @"El bananas", @"Witzi Witzi la morita", @"Golazo frijolero", @"Quesabrosillas", @"Pastamar", nil];
    
    self.titulosRecetas = array;
    
    UIGraphicsBeginImageContext (self.searchBar.frame.size);
    [[UIImage imageNamed:@"Recuadro-busqueda-side-view.png"] drawInRect:self.searchBar.bounds];
    UIImage *imageSearchBar = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
  //  [[UISearchBar appearance] setSearchFieldBackgroundImage:imageSearchBar forState:UIControlStateNormal];
    
       [self.searchBar setTranslucent:YES];
    [[UISearchBar appearance] setBackgroundImage:imageSearchBar];
    
    ///////
    //[self.searchBar setImage:[UIImage imageNamed:@"Lupa.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];

    
    self.searchBar.userInteractionEnabled = YES;
    self.searchBar.delegate = self;
    
    
    searchDisplayController = [[UISearchDisplayController alloc]
                               initWithSearchBar:self.searchBar contentsController:self];
    self.searchDisplayController.delegate = self;
    self.searchDisplayController.searchResultsDataSource = self;
    self.searchDisplayController.searchResultsDelegate = self;

     [self.searchDisplayController setActive:NO animated:YES];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    
    self.section1 = [NSArray arrayWithObjects:@"Home",@"Recetas", nil];
    
   // self.section3 = [NSArray arrayWithObjects:@"Receta1", @"Receta2", @"Receta3",@"Receta4", nil];
    
    self.section2 = [NSArray arrayWithObjects:@"Categoria1", @"Categoria2",@"Categoria3",@"Categoria4",@"Categoria5",@"Categoria6",@"FaceBook", @"Twitter",@"YouTube",@"Mas",nil];
    
  //  self.section4 = [NSArray arrayWithObjects:@"Elemento1", @"Elemento2",@"Elemento3",@"Elemento4", nil];
    
    
    self.menu = [NSArray arrayWithObjects:self.section1,self.section2,self.section3, nil];
    
    [self.slidingViewController setAnchorRightRevealAmount:265.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;

}

- (IBAction) tapBackground: (id) sender{
   // [self.searchBar resignFirstResponder];
    [self.slidingViewController anchorTopViewTo:ECRight];
    
    [self.searchBar resignFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (tableView ==self.menuTable) {
    return [self.menu count];
    }else{
         return [self.searchArray count];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView ==self.menuTable) {
    if (section == 0) {
        
        return [self.section1 count];
        
    } else if (section == 1){
        
        return [self.section2 count];
    } else if (section == 2){
        
        return 40;
    } else if (section == 3){
        
        return [self.section4 count];
    }
    }else{
    return [self.searchArray count];
    }
    
    
    return 0;

}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    
    if (section == 0) {
        
        return @"Home";
        
    } else if (section == 1) {
        
        return @"Categorias";
    } else if (section == 2){
        
        return @"Recetas";
    } else if (section == 3){
        
        return @"Lonchi-Atletas";
    }
    
    return 0;
    
    
}
- (UIView *)tableView : (UITableView *)tableView viewForHeaderInSection : (NSInteger) section {
    
    if (section == 0) {
        UIImageView *imgVew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Título-home.png"]];
    return imgVew;
    }
    if (section == 1) {
        UIImageView *imgVew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Título-categorías.png"]];
        return imgVew;
    }
    if (section == 2) {
        UIImageView *imgVew = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Título-recetas.png"]];
        return imgVew;
    }
    return 0;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (tableView ==self.menuTable) {
    NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    //cell.frame = CGRectMake(0,0,275, 55);
    // Configure the cell...
       
      UIImageView *bImg;
    if (indexPath.section == 0){
        UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
        myBackView.backgroundColor = [UIColor colorWithRed:((float)202/255.0f) green:((float)249/255.0f) blue:((float)59/255.0f) alpha:1];
        cell.selectedBackgroundView = myBackView;

    if (indexPath.row == 0) {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,320, 58)];
        }
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
        }
        
        bImg.image=[UIImage imageNamed:@"Home.png"];
        
        cell.backgroundView=bImg;
        // cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.section1 objectAtIndex:indexPath.row]];
    }
        if (indexPath.row == 1) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"NEWslideRecetas.png"];
            
            cell.backgroundView=bImg;
            
            //cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.section4 objectAtIndex:indexPath.row]];
            
        }

    }
        if (indexPath.section == 2){
            for (int i=0; i<40; i++) {
                
      if (indexPath.row == i) {
        bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
       
        bImg.image=[UIImage imageNamed:[NSString stringWithFormat: @"Lista-receta-%i.png", i+1]];
        
        cell.backgroundView=bImg;
          
      }
        
     //   cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.section2 objectAtIndex:indexPath.row]];
    }

        }
        
    if (indexPath.section == 1){
        
        if (indexPath.row == 0) {
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"Lista-categoría-1.png"];
            
            cell.backgroundView=bImg;
            
            //   cell.textLabel.text = [NSString stringWithFormat:@"%@", [self.section2 objectAtIndex:indexPath.row]];
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)0/255.0f) green:((float)171/255.0f) blue:((float)221/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        if (indexPath.row == 1) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"Lista-categoría-2.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)245/255.0f) green:((float)178/255.0f) blue:((float)9/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        
        if (indexPath.row == 2) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"Lista-categoría-3.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)160/255.0f) green:((float)10/255.0f) blue:((float)24/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        
        if (indexPath.row == 3) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
                        bImg.image=[UIImage imageNamed:@"Lista-categoría-4.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)205/255.0f) green:((float)20/255.0f) blue:((float)52/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

            
        }
        
        if (indexPath.row == 4) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"Lista-categoría-5.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)182/255.0f) green:((float)224/255.0f) blue:((float)17/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        
        if (indexPath.row == 5) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"Lista-categoría-6.png"];
            
            cell.backgroundView=bImg;
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)227/255.0f) green:((float)217/255.0f) blue:((float)25/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

            
        }
        if (indexPath.row == 6) {
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"FacebookLonchiSlide.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)33/255.0f) green:((float)103/255.0f) blue:((float)203/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        if (indexPath.row == 7) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"TwitterLonchiSlide.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)52/255.0f) green:((float)196/255.0f) blue:((float)254/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        
        if (indexPath.row == 8) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"Youtube.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)215/255.0f) green:((float)37/255.0f) blue:((float)37/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

        }
        
        if (indexPath.row == 9) {
            
            bImg= [[UIImageView alloc]initWithFrame:CGRectMake(0,0,275, 55)];
            
            bImg.image=[UIImage imageNamed:@"MásLonchiSlide.png"];
            
            cell.backgroundView=bImg;
            
            UIView *myBackView = [[UIView alloc] initWithFrame:CGRectMake(0,0,275, 55)];
            myBackView.backgroundColor = [UIColor colorWithRed:((float)218/255.0f) green:((float)137/255.0f) blue:((float)46/255.0f) alpha:1];
            cell.selectedBackgroundView = myBackView;

            
        }
       



    }
    
    if (indexPath.section == 3){
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@",[self.section4 objectAtIndex:indexPath.row]];
    }


    
    
    return cell;
     }else{
         UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Search"];
         if (cell == nil) { cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Search"]; }
         if ([searchArray count] >0) {
             cell.textLabel.text = [self.searchArray objectAtIndex:indexPath.row] ;
             UIView *backSelectedCell=[[UIView alloc ]init];
             backSelectedCell.backgroundColor=[UIColor colorWithRed:((float)44/255.0f) green:((float)87/255.0f) blue:((float)138/255.0f) alpha:1];
             cell.selectedBackgroundView=backSelectedCell;
         }
         return cell;

     }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/
////SEARCH
-(void)activateSearch{
  [self.searchBar becomeFirstResponder];
}



-(void)searchDisplayControllerDidBeginSearch:(UISearchDisplayController *)controller {
    NSLog(@"Search Started");
}

-(void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    NSLog(@"Did End Search");
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
       
    //    [self.slidingViewController resetTopView];
    //}];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    //do stuff
    
  //  [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
    
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight];
    self.menuTable.alpha =0;
    
 //   botonMenu.enabled = NO;
    
  //  self.tableView.userInteractionEnabled = NO;
    
    return YES;
}
-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
            [self.slidingViewController resetTopView];
        }];

    
  //  botonMenu.enabled = YES;
  //  [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
   // [self.slidingViewController anchorTopViewTo:ECRight];
    
  //  self.tableView.userInteractionEnabled = YES;
    NSLog(@"good");
    
   //  [self.slidingViewController anchorTopViewTo:ECRight];
    
    self.menuTable.alpha =1;
    
    [self.searchBar resignFirstResponder];
    
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar

{
    NSLog(@" searchBarCancelButtonClicked");
    
 //   botonMenu.enabled = YES;
   // [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
  //  [self.slidingViewController anchorTopViewTo:ECRight];
    
    [self.searchBar resignFirstResponder];
    
  //  self.tableView.userInteractionEnabled = YES;
    
    
  //  [self dismissModalViewControllerAnimated:YES];
    
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"Search");
    NSString *searchSt  = [NSString stringWithFormat:@"%@", self.searchBar.text];
    UIViewController *newTopViewController;

    for (int i = 0; i < [self.titulosRecetas count]; i++) {
        if ([searchSt isEqualToString:[NSString stringWithFormat:@"Receta %i", i+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"receta %i", i+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"Receta%i", i+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"receta%i", i+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@", [self.titulosRecetas objectAtIndex:i]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@", [[self.titulosRecetas objectAtIndex:i] lowercaseString]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@", [[self.titulosRecetas objectAtIndex:i] uppercaseString]]]) {
                        // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
            
            NSString * recST  = [NSString stringWithFormat:@"%i", i+1];
            
            P1.receta = recST;
            
            newTopViewController = P1;
            [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
                CGRect frame = self.slidingViewController.topViewController.view.frame;
                self.slidingViewController.topViewController = newTopViewController;
                self.slidingViewController.topViewController.view.frame = frame;
                [self.slidingViewController resetTopView];
            }];
            
            [self.searchArray addObject:[NSString stringWithFormat:@"Receta %i", i+1]];

            searchCount = 1;
        }
    }
    
    for (int k = 0; k < 4; k++) {
        if ([searchSt isEqualToString:[NSString stringWithFormat:@"Categoría %i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"categoría %i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"Categoria %i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"categoria %i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"Categoría%i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"categoría%i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"Categoria%i", k+1]]||[searchSt isEqualToString:[NSString stringWithFormat:@"categoria%i", k+1]]) {
            // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"TodasRecetas"];
            
            NSString * catST  = [NSString stringWithFormat:@"%i", k+1];
            
            P1.categoria = catST;
            
            newTopViewController = P1;
            [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
                CGRect frame = self.slidingViewController.topViewController.view.frame;
                self.slidingViewController.topViewController = newTopViewController;
                self.slidingViewController.topViewController.view.frame = frame;
                [self.slidingViewController resetTopView];
            }];
            
             [self.searchArray addObject:[NSString stringWithFormat:@"Categoría %i", k+1]];
            searchCount = 1;
        }
    }
    
    if ([searchSt isEqualToString:[NSString stringWithFormat:@"Lonchicuates"]]||[searchSt isEqualToString:[NSString stringWithFormat:@"lonchicuates"]]) {
        // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"CategoriasView"];
        
        NSString * catST  = [NSString stringWithFormat:@"5"];
        
        P1.categoria = catST;
        
        // self.recetas = rec;
        
        //  NSLog(@"%@", recST);
        
        newTopViewController = P1;
        [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
        }];
        
         [self.searchArray addObject:[NSString stringWithFormat:@"Lonchicuates"]];
        searchCount = 1;
        
        
    }
    if ([searchSt isEqualToString:[NSString stringWithFormat:@"Lonchiatletas"]]||[searchSt isEqualToString:[NSString stringWithFormat:@"lonchiatletas"]]) {
        // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"CategoriasView"];
        
        NSString * catST  = [NSString stringWithFormat:@"6"];
        
        P1.categoria = catST;
        
        // self.recetas = rec;
        
        //  NSLog(@"%@", recST);
        
        newTopViewController = P1;
        [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
        }];
        
         [self.searchArray addObject:[NSString stringWithFormat:@"Lonchiatletas"]];
        searchCount = 1;
    }


    for (int c = 0; c < [self.ingredienteAC count]; c++) {
    
        if ([searchSt isEqualToString:[NSString stringWithFormat:@"%@", [self.ingredienteAC objectAtIndex:c]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@", [self.ingrediente objectAtIndex:c]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@", [[self.ingrediente objectAtIndex:c] uppercaseString]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@", [[self.ingredienteAC objectAtIndex:c] uppercaseString]]]  ||  [searchSt isEqualToString:[NSString stringWithFormat:@"%@ ", [self.ingredienteAC objectAtIndex:c]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@ ", [self.ingrediente objectAtIndex:c]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@ ", [[self.ingrediente objectAtIndex:c] uppercaseString]]]||[searchSt isEqualToString:[NSString stringWithFormat:@"%@ ", [[self.ingredienteAC objectAtIndex:c] uppercaseString]]]) {
           
            Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
            
            
            NSString * recST  = [NSString stringWithFormat:@"%i", c];
            
            P1.ingrediente = recST;
            
            [self dismissViewControllerAnimated:YES  completion:nil];
            
            newTopViewController = P1;
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
            
             [self.searchArray addObject:[NSString stringWithFormat:@"%@", [self.ingredienteAC objectAtIndex:c]]];
            NSLog(@"%@",[NSString stringWithFormat:@"%@", [self.ingredienteAC objectAtIndex:c]]);
searchCount = 1;
            
            
        }
    }
    
    if (searchCount == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"App de Lonchicuates" message:@"No encontramos resultados, intentalo nuevamente." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];

    }
    
    [self.searchBar resignFirstResponder];
     [self.searchDisplayController setActive:NO animated:YES];
    
    NSLog(@"%@", self.searchArray);
    
    searchCount = 0;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIViewController *newTopViewController;
    
    if (indexPath.section == 0) {
        
        if ([self.searchBar isFirstResponder]) {
            [self.searchBar resignFirstResponder];
        }
       if (indexPath.row == 0) { 
        NSString *identifier = [NSString stringWithFormat:@"%@", [self.section1 objectAtIndex:indexPath.row]];
        
        //[s];
      //  NSString *identifier = [NSString stringWithFormat:@"RecetasMain"];
        
        
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
       }
        
        else  if (indexPath.row == 1) {
            
         //   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"TodasRecetas"];
            
            NSString * catST  = [NSString stringWithFormat:@"5"];
            
            P1.categoria = catST;
            //  P1.categoria= @"5";
            // self.recetas = rec;
            
            //  NSLog(@"%@", recST);
            
            newTopViewController = P1;
            
        }
        
   
        
    }
    else if (indexPath.section == 2) {
        
     //   NSString *identifier = [NSString stringWithFormat:@"%@", [self.section2 objectAtIndex:indexPath.row]];
        if ([self.searchBar isFirstResponder]) {
            [self.searchBar resignFirstResponder];
        }

        
     //   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
        
        NSString * recST  = [NSString stringWithFormat:@"%i", indexPath.row+1];
        
        P1.receta = recST;
        
       // self.recetas = rec;
        
        NSLog(@"%@", recST);
        
       //  NSString *identifier = [NSString stringWithFormat:@"RecetasMain"];
        
        newTopViewController = P1;
        
    } else if (indexPath.section == 1) {
        
        if ([self.searchBar isFirstResponder]) {
            [self.searchBar resignFirstResponder];
        }
        
        
      //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"TodasRecetas"];
        
        NSString * catST  = [NSString stringWithFormat:@"%i", indexPath.row+1];
        
        P1.categoria = catST;
      //  P1.categoria= @"5";
        // self.recetas = rec;
        
        //  NSLog(@"%@", recST);
        
        newTopViewController = P1;
        
        if (indexPath.row == 4) {
            
            
          //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"CategoriasView"];
            
            NSString * catST  = [NSString stringWithFormat:@"%i", indexPath.row+1];
            
            P1.categoria = catST;
            
            // self.recetas = rec;
            
            //  NSLog(@"%@", recST);
            
            newTopViewController = P1;
        }
        else
        if (indexPath.row == 5) {
      //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"CategoriasView"];
        
        NSString * catST  = [NSString stringWithFormat:@"%i", indexPath.row+1];
        
        P1.categoria = catST;
        
        // self.recetas = rec;
        
      //  NSLog(@"%@", recST);
        
        newTopViewController = P1;
         }
        else if (indexPath.row == 6) {
            
           // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"WEBmas"];
            
            NSString * catST  = [NSString stringWithFormat:@"https://www.facebook.com/Lonchicuates"];
            
            P1.url = catST;
            P1.procedence = @"0";
            
            // self.recetas = rec;
            
            //  NSLog(@"%@", recST);
            
            newTopViewController = P1;
        
        }
        else  if (indexPath.row == 7) {
         //   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"WEBmas"];
            
            NSString * catST  = [NSString stringWithFormat:@"https://www.twitter.com/Lonchicuates"];
            
            P1.url = catST;
             P1.procedence = @"0";
            
            // self.recetas = rec;
            
            //  NSLog(@"%@", recST);
            
            newTopViewController = P1;
            
        }
        else  if (indexPath.row == 8) {
          //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"WEBmas"];
            
            NSString * catST  = [NSString stringWithFormat:@"https://www.youtube.com/user/Lonchicuates"];
            
            P1.url = catST;
            P1.procedence = @"0";
            
            // self.recetas = rec;
            
            //  NSLog(@"%@", recST);
            
            newTopViewController = P1;
            
        }else  if (indexPath.row == 9) {
            
          //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            MasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"MasView"];
            
          //  NSString * catST  = [NSString stringWithFormat:@"https://youtube/Lonchicuates"];
            
           // P1.url = catST;
            
            // self.recetas = rec;
            
            //  NSLog(@"%@", recST);
            
            newTopViewController = P1;

            
        }
           
    } else if (indexPath.section == 3) {
        
       NSString *identifier = [NSString stringWithFormat:@"%@", [self.section4 objectAtIndex:indexPath.row]];
        
     //   NSString *identifier = [NSString stringWithFormat:@"RecetasMain"];
        newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
        
    }


    
    
    [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }];

    
    
    
    
    
    
    
}





@end
