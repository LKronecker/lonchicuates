//
//  RecetasViewController.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-08-26.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 

@interface RecetasViewController : UIViewController<UIScrollViewDelegate, MFMailComposeViewControllerDelegate>{

     UINavigationBar *bar;
    UIButton *botonMenu;
    UIScrollView *scrollView;
    UIScrollView *scrollIngredients;
    
    UIButton *faceBut;
    UIButton *twitBut;
    UIImageView *imagenReceta;
    NSTimer *timer;
    
    UILabel *navTittle;
     UILabel *recTittle;
    
    NSArray *titulosRecetas;
    NSArray *recetasArray;
    
     UIImageView * CUATE1;
    UIImageView * CUATE2;
    UIImageView * CUATE3;
    UIButton *botonMenu2;
    UIImageView * ayudaImage;
    NSString *ImageNAME;
    UIButton *botonCERRAR;
    
    int tabBarSelect;
    int nutCOunt;
    int ayudaCount;
    
    UIImageView * flechaImage;
     NSTimer *flechaTimer;
    NSArray *nutrientesArray;
    UIImageView * NUTImage;
    UIButton *mailBut;
    NSArray *urlArray;
    UITapGestureRecognizer *recoNUT;

}
@property(strong, nonatomic) IBOutlet UINavigationBar *bar;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
@property (nonatomic, retain)  UIScrollView *scrollView;
 @property (nonatomic, retain)    UIScrollView *scrollIngredients;
 @property (nonatomic, retain) IBOutlet    UILabel *navTittle;
    @property (nonatomic, retain) IBOutlet   UILabel *recTittle;
@property(nonatomic,strong)IBOutlet UIButton *faceBut;
@property(nonatomic,strong)IBOutlet UIButton *twitBut;
@property(nonatomic,strong)IBOutlet UIButton *mailBut;
  @property(nonatomic,retain)IBOutlet  NSArray *titulosRecetas;
  @property(nonatomic,retain)  NSArray *recetasArray;
@property (nonatomic, retain)  NSTimer *timer;

 @property(nonatomic,strong)IBOutlet   UIImageView *imagenReceta;

@property (nonatomic, strong) NSString *receta;
@property (nonatomic, strong) NSString *procedence;
@property (nonatomic, strong) NSString *categoST;

@property (nonatomic, strong) NSString *ImageNAME;

@property(strong, nonatomic) IBOutlet UIImageView * CUATE1;
@property(strong, nonatomic) IBOutlet UIImageView * CUATE2;
@property(strong, nonatomic) IBOutlet UIImageView * CUATE3;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu2;
@property(nonatomic,strong)IBOutlet  UIImageView * ayudaImage;
@property(nonatomic,strong)IBOutlet UIButton *botonCERRAR;

@property(strong, nonatomic) IBOutlet UIImageView * flechaImage;
@property (nonatomic, retain)  NSTimer *flechaTimer;
@property(nonatomic,retain)  NSArray *nutrientesArray;
@property(nonatomic,retain)  NSArray *urlArray;
@property(nonatomic,strong)IBOutlet  UIImageView * NUTImage;

@property(nonatomic,strong)IBOutlet   UITapGestureRecognizer *recoNUT;


-(IBAction)click;
-(IBAction)clickMas;
-(IBAction)TWpost;
-(IBAction)FBpost;
- (IBAction)exitAction:(id)sender;
-(IBAction)slideBack:(id)sender;
-(IBAction)slideMas:(id)sender;
-(IBAction)pushMail;
-(IBAction)clickTodasRece;
///animaciones
- (IBAction)BegginAnimaconFlecha:(id)sender;
- (IBAction)EndAnimaconFlecha:(id)sender;

- (IBAction)BegginAnimaconNutrientes:(id)sender;
- (IBAction)EndAnimaconNutrientes:(id)sender;
////Tab Bar

-(IBAction)buscar;
-(IBAction)tapIngredient;
-(IBAction)ayudaCERRAR;
-(IBAction)postMail;



@property(nonatomic,strong)IBOutlet  UIImageView * Ingre1;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre2;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre3;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre4;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre5;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre6;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre7;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre8;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre9;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre10;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre11;
@property(nonatomic,strong)IBOutlet  UIImageView * Ingre12;
@end
