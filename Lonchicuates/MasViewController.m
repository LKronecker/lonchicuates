//
//  MasViewController.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-10.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "MasViewController.h"
#import "ECSlidingViewController.h"
#import "MainViewController.h"
#import "WEBmasViewController.h"

@interface MasViewController ()

@end

@implementation MasViewController
@synthesize bar, botonMenu, timer, mainView, webView, scrollView, timerSlide, scrollViewAv, avisoST, CUATE3, botonMenu2, ayudaImage, activityIndicator, botonCERRAR;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    avisoBOOL = 0;
    
    self.botonCERRAR.alpha = 0;
    self.ayudaImage.alpha=0;
    self.CUATE3.alpha = 0;
        
	// Do any additional setup after loading the view.
   
    
     self.bar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];
    
    UIImage *menuImg=[UIImage imageNamed:@"Botón-regresarNew.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [bar pushNavigationItem:navItem animated:NO];
    
    ///
    
    UIImage *menuImg2=[UIImage imageNamed:@"Botton-ayuda.png"];
    self.botonMenu2 =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu2.frame=CGRectMake(279, 8,  40, 35);
    [self.botonMenu2 setBackgroundImage:menuImg2 forState:UIControlStateNormal];
    [self.botonMenu2  addTarget:self action:@selector(ayuda) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem2= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu2];
    //  UINavigationItem *navItem2=[[UINavigationItem alloc] init];
    navItem.rightBarButtonItem=menuItem2;
    [ self.navigationController.navigationBar addSubview:botonMenu2];
    
    ///
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
         self.scrollViewAv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, -1004, 768, 1004)];
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
    self.scrollViewAv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, -480, 320, 480)];
    }
    [self.view addSubview: self.scrollViewAv];
    CGSize scrollViewContentSize = CGSizeMake(320, 600);
    [self.scrollViewAv setContentSize:scrollViewContentSize];
    scrollViewAv.delegate = self;
    
    [self.scrollViewAv setBackgroundColor:[UIColor blackColor]];
    [scrollViewAv setCanCancelContentTouches:NO];
    
    scrollViewAv.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollViewAv.clipsToBounds = YES;
    scrollViewAv.scrollEnabled = YES;
    scrollViewAv.pagingEnabled = NO;
    scrollViewAv.alpha = 0;
    scrollViewAv.backgroundColor = nil;
    
    scrollViewAv.scrollsToTop = YES;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        NSString *imageName = [NSString stringWithFormat:@"Tezto-aviso-de-privacidad-ipad.png"];
        self.avisoST = imageName;
       
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        NSString *imageName = [NSString stringWithFormat:@"Texto-aviso-de-privacidad.png"];
   self.avisoST = imageName;
    }
    
    UIImage *image = [UIImage imageNamed:self.avisoST];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
               
        CGRect rect = imageView.frame;
        rect.size.height = 1004;
        rect.size.width = 768;
        rect.origin.x = 0;
        
        rect.origin.y = 0;
        imageView.frame = rect;
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
       
        
            CGRect rect = imageView.frame;
            rect.size.height = 700;
            rect.size.width = 320;
            rect.origin.x = 0;
    
            rect.origin.y = 0;
            imageView.frame = rect;
    }
    
            //   NSLog(@"%i, %f %f", drawCount, cx, imageView.frame.origin.x);
            imageView.userInteractionEnabled = YES;
    
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
            /////Tap Recognizer
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(AvisoBack)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            
            [imageView addGestureRecognizer:recognizer];
            ///////////
            
            [scrollViewAv addSubview:imageView];
            // [imageView release];
    
    scrollViewAv.maximumZoomScale = 4.0;
    scrollViewAv.minimumZoomScale = 1.0;
            
           
    [scrollViewAv setContentSize:CGSizeMake(imageView.frame.size.width, 790)];
    
 //////////////
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, 320, 420)];
            
        }
        
        else if (iOSDeviceScreenSize.height == 568){
            self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, 320, 548)];
        }
    }
    
    
    [self.view addSubview: self.scrollView];
    CGSize scrollViewContentSize1 = CGSizeMake(320, 600);
    [self.scrollView setContentSize:scrollViewContentSize1];
    scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = NO;
    scrollView.alpha = 1;
    scrollView.backgroundColor = nil;
    
    scrollView.scrollsToTop = YES;
    ////
    
       
       ////
    UIImageView *imageMFUerte= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Acerca-de.png"]]];
    imageMFUerte.frame = CGRectMake(6, 8, 150, 130);
    
    
    imageMFUerte.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasfuerte = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(acercaDE)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMFUerte addGestureRecognizer:recogMasfuerte];
    
    [scrollView addSubview:imageMFUerte];
    //////
    UIImageView *imageMListo= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Aviso-de-privacidad.png"]]];
    imageMListo.frame = CGRectMake(164, 8, 150, 130);
    
    
    imageMListo.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasLis = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(aviso)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMListo addGestureRecognizer:recogMasLis];
    
    [scrollView addSubview:imageMListo];
    /////
    
    UIImageView *imageMEnergia= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Ayuda.png"]]];
    imageMEnergia.frame = CGRectMake(6, 146, 150, 130);
    
    
    imageMEnergia.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasEner = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MASayuda)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMEnergia addGestureRecognizer:recogMasEner];
    
    [scrollView addSubview:imageMEnergia];
    ///
    /////
    UIImageView *imageMLiSan= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Sitio.png"]]];
    imageMLiSan.frame = CGRectMake(164, 146, 150, 130);
    
    
    imageMLiSan.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasSan = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(acercaSitio)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMLiSan addGestureRecognizer:recogMasSan];
    
    [scrollView addSubview:imageMLiSan];
    /////
    
    UIImageView *imageCuates= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Facebook.png"]]];
    imageCuates.frame = CGRectMake(6   , 284, 150, 130);
    
    
    imageCuates.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogCUa = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(FaceBook)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageCuates addGestureRecognizer:recogCUa];
    
    [scrollView addSubview:imageCuates];
    /////
    UIImageView *imageAtletas= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Twitter.png"]]];
    imageAtletas.frame = CGRectMake(164  , 284, 150, 130);
    
    
    imageAtletas.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogATL = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Twitter)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageAtletas addGestureRecognizer:recogATL];
    
    [scrollView addSubview:imageAtletas];
    /////
    
    UIImageView *imageFB= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MainViewYoutube.png"]]];
    imageFB.frame = CGRectMake(6  , 422, 150, 130);
    
    
    imageFB.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogFB = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(YouTube)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageFB addGestureRecognizer:recogFB];
    
    [scrollView addSubview:imageFB];
    /////
    
    UIImageView *imageTW= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Contáctanos.png"]]];
    imageTW.frame = CGRectMake(164  , 422, 150, 130);
    
    
    imageTW.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogTW = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(contactanos)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageTW addGestureRecognizer:recogTW];
    
    [scrollView addSubview:imageTW];
    /////
       
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            [scrollView setContentSize:CGSizeMake(320, 570)];
            
        }
        
        else if (iOSDeviceScreenSize.height == 568){
            [scrollView setContentSize:CGSizeMake(320, 650)];
        }
    }

    UITapGestureRecognizer *recognizerAY = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ayudaCERRAR)];
    recognizerAY.numberOfTapsRequired = 1;
    recognizerAY.numberOfTouchesRequired = 1;
    self.ayudaImage.userInteractionEnabled = YES;
    [self.ayudaImage addGestureRecognizer:recognizerAY];
}

-(IBAction)ayuda{
    self.scrollView.alpha = 0;
    
    self.botonCERRAR.alpha = 1;
    
    //self.scrollView.alpha = 0;
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    self.ayudaImage.alpha = 1;
    self.CUATE3.alpha = 1;
    [UIView commitAnimations];
    
    [self.view bringSubviewToFront:ayudaImage];
    [self.view bringSubviewToFront:CUATE3];
    [self.view bringSubviewToFront:botonCERRAR];
    
    self.navigationController.navigationBarHidden = YES;
    //   self.opcionesView.userInteractionEnabled = NO;
    //  self.recetaView.userInteractionEnabled = NO;
    NSLog(@"ayuda");
}
-(IBAction)ayuda2{
    
    
    //  self.scrollView.alpha = 1;
    
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.ayudaImage.alpha = 0;
    
    self.CUATE3.alpha = 0;
    [UIView commitAnimations];
    
    
    //   self.opcionesView.userInteractionEnabled = YES;
    //  self.recetaView.userInteractionEnabled = YES;
    NSLog(@"ayuda2");
    self.navigationController.navigationBarHidden = NO;
    [ self.navigationController.navigationBar addSubview:botonMenu];
    [ self.navigationController.navigationBar addSubview:botonMenu2];
}
-(IBAction)ayudaCERRAR{
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.ayudaImage.alpha = 0;
    self.botonCERRAR.alpha = 0;
    self.CUATE3.alpha = 0;
    self.scrollView.alpha = 1;
    [UIView commitAnimations];
    
    
    self.navigationController.navigationBarHidden = NO;
    [ self.navigationController.navigationBar addSubview:botonMenu];
    [ self.navigationController.navigationBar addSubview:botonMenu2];
    
    NSLog(@"ayudaCERRAR");
}


-(IBAction)MASayuda{
    UIViewController *newTopViewController;
    WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    // if (lonchiCOunt ==1) {
    
    NSString * recST  = [NSString stringWithFormat:@"http://goo.gl/dTDawH"];
    
    P1.url = recST;
    P1.procedence = @"1";
    //  }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
    
}



-(IBAction)acercaDE{
     UIViewController *newTopViewController;
    WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
   // if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"http://hablemosdeazucar.com.mx/"];
        
        P1.url = recST;
    P1.procedence = @"1";
  //  }
        
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];


}

-(IBAction)acercaSitio{
    UIViewController *newTopViewController;
    WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    // if (lonchiCOunt ==1) {
    
    NSString * recST  = [NSString stringWithFormat:@"http://www.lonchicuates.com.mx/"];
    
    P1.url = recST;
     P1.procedence = @"1";
    //  }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
    
}

-(IBAction)contactanos{
       MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    NSArray *recipients = [[NSArray alloc]initWithObjects:@"contacto@lonchicuates.com.mx", nil];
    [picker setToRecipients:recipients];
    [picker setSubject:[NSString stringWithFormat:@"Dinos lo que piensas"]];
    [picker setMessageBody:[NSString stringWithFormat:@":)"] isHTML:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(IBAction)aviso{
    avisoBOOL =1;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.6];
    
    
    CGRect viewFrame = self.mainView.frame;
    CGRect recetFrame = self.scrollView.frame;
     CGRect recetFrame1 = self.scrollViewAv.frame;
    //  CGRect tableFrame = indcatorsTable.frame;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        viewFrame.origin.y += 1004;
        recetFrame.origin.y += 1100;
        recetFrame1.origin.y += 930;
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
    viewFrame.origin.y += 480;
    recetFrame.origin.y += 540;
    recetFrame1.origin.y += 540;
    }
    //   tableFrame.origin.x += 270;
    
    self.mainView.frame = viewFrame;
    self.scrollView.frame =recetFrame;
    self.scrollViewAv.frame = recetFrame1;
      //indcatorsTable.frame = tableFrame;
    
     self.scrollViewAv.alpha = 1;
    
    
    [UIView commitAnimations];
    [self.view bringSubviewToFront:botonMenu2];
}

-(IBAction)AvisoBack{
    avisoBOOL=0;
      self.scrollViewAv.alpha = 0;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.6];
    
    
    CGRect viewFrame = self.mainView.frame;
    CGRect recetFrame = self.scrollView.frame;
    CGRect recetFrame1 = self.scrollViewAv.frame;
    //  CGRect tableFrame = indcatorsTable.frame;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        viewFrame.origin.y -= 1004;
        recetFrame.origin.y -= 1100;
        recetFrame1.origin.y -= 930;
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        viewFrame.origin.y -= 480;
        recetFrame.origin.y -= 540;
        recetFrame1.origin.y -= 540;
    }

    //   tableFrame.origin.x += 270;
    
    self.mainView.frame = viewFrame;
    self.scrollView.frame =recetFrame;
    self.scrollViewAv.frame = recetFrame1;
    //indcatorsTable.frame = tableFrame;    //  indcatorsTable.frame = tableFrame;
    
    
    
    
    [UIView commitAnimations];

//self.scrollView.alpha = 0;
  //  self.mainView.alpha = 1;
}

-(IBAction)click
{
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    
   // self.mainView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
    
}

-(IBAction)slideBack:(id)sender{
    
    if (avisoBOOL==0) {
        
    
    UIViewController *newTopViewController;
     //   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        MainViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
        
        avisoBOOL = 1;
    }else{
        [self AvisoBack];
        avisoBOOL = 0;
          }
}
-(IBAction)FaceBook{
    categoriaCount = 7;
    socialBool = 0;
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
  //  self.opcionesView.alpha = 0;
   // self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}

-(IBAction)Twitter{
    categoriaCount = 7;
    socialBool = 1;
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
 //   self.opcionesView.alpha = 0;
  //  self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}

-(IBAction)YouTube{
    categoriaCount = 7;
    socialBool = 2;
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
   // self.opcionesView.alpha = 0;
   // self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}

-(IBAction)slideBack2:(id)sender{
    if (categoriaCount ==7) {
        
        
        UIViewController *newTopViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier: @"WEBmas"];
        
        P1.procedence = @"1";
        
        if (socialBool == 0) {
            NSString * catST  = [NSString stringWithFormat:@"https://www.facebook.com/Lonchicuates"];
            P1.url = catST;
            
        }
        
        if (socialBool == 1) {
            NSString * catST  = [NSString stringWithFormat:@"https://www.twitter.com/Lonchicuates"];
            P1.url = catST;
        }
        if (socialBool == 2) {
            NSString * catST  = [NSString stringWithFormat:@"https://www.youtube.com/user/Lonchicuates"];
            P1.url = catST;
        }
        
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
        NSLog(@"hheeeey");
    }
    
        
    //}];
    
    
    //  [self.slidingViewController resetTopView];
}


- (IBAction)exitAction:(id)sender {
    [self dismissViewControllerAnimated:YES  completion:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"AcercaDE"]) {
       WEBmasViewController  *destViewController = segue.destinationViewController;
         destViewController.url=@"http://hablemosdeazucar.com.mx/";
        
    }else if ([segue.identifier isEqualToString:@"sitio"]) {
        WEBmasViewController  *destViewController = segue.destinationViewController;
        destViewController.url=@"http://www.lonchicuates.com.mx/";
        
    }
    else if ([segue.identifier isEqualToString:@"goToFB"]) {
        WEBmasViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.facebook.com/Lonchicuates";
        
              
    }
    else if ([segue.identifier isEqualToString:@"goToTwittwer"]) {
        WEBmasViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.twitter.com/Lonchicuates";
              
        
    }
    else if ([segue.identifier isEqualToString:@"goToYoutube"]) {
        WEBmasViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.youtube.com/user/Lonchicuates";
        
        
    }

}

@end
