//
//  DetailSocialViewController.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-08-27.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "DetailSocialViewController.h"

@interface DetailSocialViewController ()

@end

@implementation DetailSocialViewController
@synthesize socialWeb;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIWebView *web =[[UIWebView alloc]init];
    
    web.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
    web.scalesPageToFit=YES;
    [web setDelegate:self];
    [self.view addSubview:web];
    self.socialWeb = web;
    UIActivityIndicatorView *actInd=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actInd.color=[UIColor blackColor];
    
    [actInd setCenter:self.view.center];
    self.activityIndicator=actInd;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
        {
            web.frame = CGRectMake(0, 44, 768, 960);
        }
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation))
        {
            orientseason = 1;
            self.headOutlet.frame= CGRectMake(0, 0, 1024, 44);
            web.frame = CGRectMake(0, 44, 1024, 722);
        }
        
        
    }else{
        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            web.frame = CGRectMake(0, 44, 320, 416);
        }else{
            web.frame = CGRectMake(0, 44, 320, 510);
            
        }
    }
    
    
    //Add the indicator to the webView to make it visible
    [web addSubview:self.activityIndicator];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator startAnimating];
    
}
- (void)viewDidAppear:(BOOL)animated{
    [self.socialWeb loadRequest:[NSURLRequest requestWithURL:
                                 [NSURL URLWithString:[self url]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDetailSocialWebOutlet:nil];
    [self setHeadOutlet:nil];
    [super viewDidUnload];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self.socialWeb setHidden:YES];
    //[self.activityIndicator stopAnimating];
    [self.socialWeb setHidden:NO];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
    [self.socialWeb setHidden:NO];
}
- (IBAction)exitAction:(id)sender {
    [self dismissViewControllerAnimated:YES  completion:nil];
}

@end
