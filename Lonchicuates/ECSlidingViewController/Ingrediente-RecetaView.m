//
//  Ingrediente-RecetaView.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-12.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "Ingrediente-RecetaView.h"
#import "ECSlidingViewController.h"
#import "MainViewController.h"
#import "RecetasViewController.h"

@interface Ingrediente_RecetaView ()

@end

@implementation Ingrediente_RecetaView
@synthesize receIng, scrollView, bar, botonMenu, timer, tittleLabel, ingredienteAC;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray *ingArray = [[NSArray alloc]initWithObjects:@"Aceite de olivo", @"Aceite en aerosol", @"Agua de mango", @"Agua simple", @"Aguacate", @"Ajo", @"Almendras", @"Apio", @"Arándanos", @"Aros de manzana", @"Arroz cocido", @"Arroz blanco", @"Azúcar", @"Bolillo", @"Brócoli", @"Cacahuates", @"Cacahuates enchilados", @"Camarones", @"Canela", @"Canela en polvo", @"Cebolla", @"Claras de huevo", @"Cereal de trigo", @"Champiñones", @"Chile piquín", @"Chips de maíz", @"Chocolate", @"Coctel de frutas", @"Crema", @"Crema de cacahuate", @"Cuadritos de queso", @"Espinaca", @"Esquites", @"Fresas", @"Frijoles molidos", @"Frijoles refritos", @"Frambuesas", @"Fusilli", @"Garbanzos", @"Galletas de animalitos", @"Galletas de chocolate o vainilla", @"Galletas habaneras", @"Galletas marías", @"Galletas saladas integrales", @"Gelatina", @"Granola", @"Hot cakes", @"Huevo", @"Jamaica", @"Jamón de pavo", @"Jitomate", @"Jitomate cherry", @"Jicama", @"Jocoque", @"Jugo de naranja", @"Kiwi", @"Leche", @"Lechuga", @"Lechuga romana", @"Limón", @"Mandarínas", @"Mango", @"Mantequilla", @"Manzana verde o roja", @"Mayonesa", @"Melón", @"Melón verde", @"Mermelada de fresa", @"Mermelada de zarzamora", @"Miel de abeja", @"Miel de maple", @"Mostaza", @"Moras", @"Naranja", @"Nueces", @"Palitos de pan", @"Palitos de pepino", @"Papaya", @"Palillos de papel o plástico", @"Plátano", @"Pan integral", @"Pan molido", @"Papa", @"Pasta de chocolate (nutella)", @"Pasta de jitomate", @"Pechuga de pollo", @"Pechuga de pavo", @"Pepino", @"Pera", @"Pimienta", @"Piña", @"Piña en almibar", @"Pretzels", @"Puré de tomate", @"Queso cheddar", @"Queso crema", @"Queso oaxaca", @"Queso manchego", @"Queso mozzarella", @"Queso panela", @"Queso philadelphia", @"Requesón", @"Sal", @"Sal de ajo", @"Salsa de soya", @"Sazonador en polvo sabor mantequilla", @"Sopes", @"Surimi", @"Toronja", @"Tortillas de harina", @"Tostadas horneadas", @"Toalla de papel", @"Uva", @"Yoghurt natural", @"Zanahorias baby", @"Zarzamoras", @"Té helado", @"Sandía", @"Nopales", @"Palomitas", @"Zanahoria rallada", @"Agua de limón", @"Rollitos de jamón", @"Uvas con ate", @"Rebanada de jamón", @"Agua de naranja", @"Fresas con yoghurt", @"Manzana", @"Zanahoria", @"Vinagre balsámico", @"Tostadas", @"Bolsita de té de frutas", @"Tortillas de maíz", nil];
    self.ingredienteAC =ingArray;
    
    self.tittleLabel.text = [NSString stringWithFormat:@"%@", [self.ingredienteAC objectAtIndex:[self.ingrediente intValue]]];
    
	// Do any additional setup after loading the view.
    self.bar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];

    
    UIImage *menuImg=[UIImage imageNamed:@"Botón-regresarNew.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [bar pushNavigationItem:navItem animated:NO];
    /////
    if ([self.ingrediente isEqualToString:@"0"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-24.png", @"principal-receta-27.png",  nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"1"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-33.png", @"principal-receta-40.png", nil];
        self.receIng =array; }

    if ([self.ingrediente isEqualToString:@"2"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-19.png", nil];
        self.receIng =array; }

    if ([self.ingrediente isEqualToString:@"3"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", @"principal-receta-3.png", @"principal-receta-4.png", @"principal-receta-5.png", @"principal-receta-8.png", @"principal-receta-13.png", @"principal-receta-16.png", @"principal-receta-18.png", @"principal-receta-20.png", @"principal-receta-21.png", @"principal-receta-22.png", @"principal-receta-23.png", @"principal-receta-24.png", @"principal-receta-26.png", @"principal-receta-27.png", @"principal-receta-28.png", @"principal-receta-29.png", @"principal-receta-30.png", @"principal-receta-31.png", @"principal-receta-32.png", @"principal-receta-33.png", @"principal-receta-34.png", @"principal-receta-35.png", @"principal-receta-36.png", @"principal-receta-38.png", @"principal-receta-39.png", nil];
        self.receIng =array; }

    if ([self.ingrediente isEqualToString:@"4"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", @"principal-receta-12.png", @"principal-receta-20.png", @"principal-receta-30.png", @"principal-receta-33.png", @"principal-receta-34.png", @"principal-receta-38.png", @"principal-receta-39.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"5"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"6"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-2.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"7"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"8"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-25.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"9"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-31.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"10"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"11"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"12"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", @"principal-receta-23.png", @"principal-receta-26.png", @"principal-receta-30.png", @"principal-receta-32.png", @"principal-receta-35.png", @"principal-receta-36.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"13"]) {
        NSArray *array = [[NSArray alloc]initWithObjects: @"principal-receta-17.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"14"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-18.png", @"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"15"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-18.png", @"principal-receta-25.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"16"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-28.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"17"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"18"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"19"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", @"principal-receta-36.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"20"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", @"principal-receta-39.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"21"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-26.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"22"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-2.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"23"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-19.png", @"principal-receta-21.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"24"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", @"principal-receta-6.png", @"principal-receta-14.png", @"principal-receta-15.png", @"principal-receta-31.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"25"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-4.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"26"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-25.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"27"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-8.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"28"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"29"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-14.png", @"principal-receta-29.png", @"principal-receta-31.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"30"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-24.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"31"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-4.png", @"principal-receta-8.png", @"principal-receta-13.png", @"principal-receta-26.png", @"principal-receta-39.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"32"]) {
        NSArray *array = [[NSArray alloc]initWithObjects: @"principal-receta-7.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"33"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-13.png", @"principal-receta-14.png", @"principal-receta-37.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"34"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-4.png", @"principal-receta-5.png", @"principal-receta-10.png", @"principal-receta-17.png", @"principal-receta-22.png", @"principal-receta-32.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"35"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-4.png", @"principal-receta-5.png", @"principal-receta-10.png", @"principal-receta-17.png", @"principal-receta-22.png", @"principal-receta-32.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"36"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-37.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"37"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-18.png", @"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"38"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-24.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"39"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-28.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"40"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"41"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-15.png", @"principal-receta-36.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"42"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-8.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"43"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"44"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-16.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"45"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-5.png", @"principal-receta-18.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"46"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-21.png", @"principal-receta-31.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"47"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", @"principal-receta-4.png", @"principal-receta-35.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"48"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-30.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"49"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-7.png", @"principal-receta-11.png", @"principal-receta-30.png", @"principal-receta-36.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"50"]) {
        NSArray *array = [[NSArray alloc]initWithObjects: @"principal-receta-1.png", @"principal-receta-4.png", @"principal-receta-19.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"51"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-27.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"52"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", @"principal-receta-15.png", @"principal-receta-24.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"53"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-15.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"54"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-8.png", @"principal-receta-23.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"55"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-21.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"56"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-25.png", nil];
        self.receIng =array; }
    
    if ([self.ingrediente isEqualToString:@"57"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-19.png", @"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"58"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-19.png", @"principal-receta-32.png", nil];
        self.receIng =array; }
    
    if ([self.ingrediente isEqualToString:@"59"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", @"principal-receta-19.png", @"principal-receta-24.png", @"principal-receta-26.png", @"principal-receta-31.png", @"principal-receta-33.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    
    if ([self.ingrediente isEqualToString:@"60"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    
    if ([self.ingrediente isEqualToString:@"61"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-11.png", @"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"62"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"63"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-2.png", @"principal-receta-15.png", @"principal-receta-23.png", @"principal-receta-32.png", @"principal-receta-35.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"64"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-30.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"65"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", @"principal-receta-18.png", @"principal-receta-27.png", @"principal-receta-29.png", @"principal-receta-39.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"66"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-29.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"67"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-14.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"68"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-16.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"69"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-18.png", @"principal-receta-27.png", @"principal-receta-29.png", @"principal-receta-35.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"70"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-21.png", @"principal-receta-31.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"71"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-30.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"72"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-37.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"73"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-6.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"74"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-17.png", @"principal-receta-29.png", @"principal-receta-35.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"75"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-20.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"76"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-11.png", @"principal-receta-19.png", @"principal-receta-27.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"77"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-29.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"78"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-29.png", @"principal-receta-37.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"79"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-5.png", @"principal-receta-29.png", @"principal-receta-30.png", @"principal-receta-36.png", @"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"80"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", @"principal-receta-8.png", @"principal-receta-12.png", @"principal-receta-14.png", @"principal-receta-16.png", @"principal-receta-19.png", @"principal-receta-20.png", @"principal-receta-29.png", @"principal-receta-30.png", @"principal-receta-35.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"81"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-33.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"82"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-33.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"83"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-13.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"84"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"85"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", @"principal-receta-33.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"86"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-23.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"87"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-11.png", @"principal-receta-19.png", @"principal-receta-27.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"88"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-17.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"89"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-26.png", @"principal-receta-33.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"90"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-14.png", @"principal-receta-17.png", @"principal-receta-29.png", @"principal-receta-33.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"91"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-17.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"92"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-25.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"93"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-19.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"94"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-30.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"95"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", @"principal-receta-23.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"96"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-6.png", @"principal-receta-10.png", @"principal-receta-11.png", @"principal-receta-17.png", @"principal-receta-19.png", @"principal-receta-21.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"97"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-8.png", @"principal-receta-18.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"98"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-26.png", @"principal-receta-27.png", @"principal-receta-36.png", @"principal-receta-39.png", @"principal-receta-40.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"99"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png", @"principal-receta-5.png", @"principal-receta-12.png", @"principal-receta-13.png", @"principal-receta-22.png", @"principal-receta-28.png", @"principal-receta-31.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"100"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"101"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-16.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"102"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", @"principal-receta-18.png", @"principal-receta-26.png", @"principal-receta-27.png", @"principal-receta-33.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"103"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-24.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"104"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"105"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-33.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"106"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-22.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"107"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-9.png", @"principal-receta-34.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"108"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-20.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"109"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-6.png", @"principal-receta-10.png", @"principal-receta-11.png", @"principal-receta-13.png", @"principal-receta-38.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"110"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"111"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@" ", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"112"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-28.png", @"principal-receta-10.png", @"principal-receta-29.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"113"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-2.png", @"principal-receta-5.png",  @"principal-receta-12.png", @"principal-receta-29.png", @"principal-receta-37.png", nil];
        self.receIng =array; }
    
    if ([self.ingrediente isEqualToString:@"114"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-7.png", @"principal-receta-22.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"115"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-16.png", @"principal-receta-37.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"116"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-2.png", @"principal-receta-9.png", @"principal-receta-10.png", @"principal-receta-12.png", @"principal-receta-14.png", @"principal-receta-17.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"117"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-3.png", @"principal-receta-17.png", @"principal-receta-27.png", @"principal-receta-29.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"118"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-5.png", @"principal-receta-6.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"119"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@" ", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"120"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-6.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"121"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-6.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"122"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-7.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"123"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-10.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"124"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-11.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"125"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-11.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"126"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-12.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"127"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-15.png", @"principal-receta-23.png", @"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"128"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-15.png", @"principal-receta-20.png", @"principal-receta-24.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"129"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-27.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"130"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"131"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-32.png", nil];
        self.receIng =array; }
    if ([self.ingrediente isEqualToString:@"132"]) {
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-39.png", nil];
        self.receIng =array; }

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(6, 50, 768, 1024)];
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(6, 50, 320, 505)];
    }

    
    [self.view addSubview: self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake(320, 600);
    [self.scrollView setContentSize:scrollViewContentSize];
    scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = NO;
    scrollView.alpha = 1;
    scrollView.backgroundColor = nil;
    
    scrollView.scrollsToTop = YES;
    
    NSUInteger nimages = 1;
    CGFloat cx = 0;
    CGFloat cy = 0;
    for (;nimages<15 ; nimages++) {
        int drawCount = 0;
        for (int k = 0 + 3*(nimages-1); k < 3*(nimages); k ++) {
            
            if (k < [self.receIng count]) {
                
            NSString *imageName = [NSString stringWithFormat:@"%@", [self.receIng objectAtIndex:k]];
            
            
            UIImage *image = [UIImage imageNamed:imageName];
            
            
            if (image == nil    ) {
                break;
            }
            
            
            // UIImageView *imageView=(UIImageView *)[self.view viewWithTag:k];
            //       [imageView setImage:image];
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-1.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap1:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-2.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap2:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-3.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap3:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-4.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap4:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-5.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap5:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-6.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap6:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-7.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap7:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-8.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap8:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-9.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap9:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-10.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap10:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-11.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap11:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-12.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap12:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-13.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap13:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-14.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap14:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-15.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap15:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-16.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap16:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-17.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap17:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-18.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap18:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-19.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap19:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-20.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap20:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-21.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap21:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-22.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap22:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-23.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap23:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-24.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap24:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-25.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap25:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-26.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap26:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-27.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap27:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-28.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap28:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-29.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap29:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-30.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap30:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-31.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap31:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-32.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap32:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-33.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap33:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-34.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap34:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-35.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap35:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-36.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap36:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-37.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap37:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-38.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap38:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-39.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap39:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-40.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap40:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];
                
            }
            
            
            
            
            
            
            
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                    CGRect rect = imageView.frame;
                    rect.size.height = 250;
                    rect.size.width = 250;
                    rect.origin.x = ((scrollView.frame.size.width - (image.size.width))*(drawCount)/2) +cx;
                    
                    rect.origin.y = 250*(nimages-1) + cy;
                    imageView.frame = rect;
                    
                }
                
                
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                    CGRect rect = imageView.frame;
                    rect.size.height = 100;
                    rect.size.width = 100;
                    rect.origin.x = ((scrollView.frame.size.width - (image.size.width))*(drawCount)) +cx;
                    
                    rect.origin.y = 100*(nimages-1) + cy;
                    imageView.frame = rect;
                    
                }
            
            //   NSLog(@"%i, %f %f", drawCount, cx, imageView.frame.origin.x);
            
            
            ///////////
            
            [scrollView addSubview:imageView];
            // [imageView release];
            }
            
            drawCount = (drawCount+1)%4;
            
            if (drawCount < 3) {
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                    cx += scrollView.frame.size.width/30;
                }
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                    cx += scrollView.frame.size.width/3.3;
                }

            } else{
                cx = 0;
            }
            
            
            
            
        }
        
        cy += scrollView.frame.size.height/80;
        
    }
    
    
    [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+15*100)];
    
    
    ////



    
}

-(IBAction)click
{
    [self dismissViewControllerAnimated:YES  completion:nil];
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    
    //  self.mainView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
    
}

-(IBAction)slideBack:(id)sender{    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UIViewController *newTopViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
        
        
    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            
            UIViewController *newTopViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
        }
        else if (iOSDeviceScreenSize.height == 568){
            
            UIViewController *newTopViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone5" bundle:nil];
            MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
            
        }
    }
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(IBAction)receTap1:(id)sender{
    
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"1"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)receTap2:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"2"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap3:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"3"];
    
    P1.receta = recST;
   // P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap4:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"4"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap5:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"5"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap6:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"6"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)receTap7:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"7"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap8:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"8"];
    
    P1.receta = recST;
  //  P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap9:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"9"];
    
    P1.receta = recST;
   // P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)receTap10:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"10"];
    
    P1.receta = recST;
   // P1.procedence = @"2";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap11:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"11"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap12:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"12"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap13:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"13"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap14:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"14"];
    
    P1.receta = recST;
   // P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap15:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"15"];
    
    P1.receta = recST;
 //   P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap16:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"16"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap17:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"17"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap18:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"18"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap19:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"19"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap20:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"20"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap21:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"21"];
    
    P1.receta = recST;
   // P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap22:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"22"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap23:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"23"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap24:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"24"];
    
    P1.receta = recST;
   // P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap25:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"25"];
    
    P1.receta = recST;
   // P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap26:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"26"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap27:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"27"];
    
    P1.receta = recST;
 //   P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap28:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"28"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap29:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"29"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap30:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"30"];
    
    P1.receta = recST;
 //   P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap31:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"31"];
    
    P1.receta = recST;
 //   P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap32:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"32"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)receTap33:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"33"];
    
    P1.receta = recST;
 //   P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap34:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"34"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap35:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"35"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap36:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"36"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap37:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"37"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap38:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"38"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap39:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"39"];
    
    P1.receta = recST;
  //  P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap40:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"40"];
    
    P1.receta = recST;
   // P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}




@end
