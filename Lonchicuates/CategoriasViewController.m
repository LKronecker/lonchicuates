//
//  CategoriasViewController.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-08.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "CategoriasViewController.h"
#import "ECSlidingViewController.h"
#import "MainViewController.h"
#import "RecetasViewController.h"
#import "WEBmasViewController.h"

@interface CategoriasViewController ()

@end

@implementation CategoriasViewController
@synthesize bar, botonMenu, timer, categoria, scrollView, navTittle, imageName, displayImage, CatImage, lonchiAtletas, lonchiCuates;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.3];
    
    self.scrollView.alpha = 1;
    
    
    [UIView commitAnimations];
    
  
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray * cuates =[[NSArray alloc]initWithObjects:@"http://www.lonchicuates.com.mx/ana-vellana.html", @"http://www.lonchicuates.com.mx/animoritas.html", @"http://www.lonchicuates.com.mx/broco-dance.html", @"http://www.lonchicuates.com.mx/celia.html", @"http://www.lonchicuates.com.mx/champier-y-chamcarlo.html", @"http://www.lonchicuates.com.mx/chill--guito.html", @"http://www.lonchicuates.com.mx/dominico.html", @"http://www.lonchicuates.com.mx/don-tomatoacuten.html", @"http://www.lonchicuates.com.mx/dontildea-mandarina.html", @"http://www.lonchicuates.com.mx/fidel-y-rauacutel.html", @"http://www.lonchicuates.com.mx/fortatuacuten.html", @"http://www.lonchicuates.com.mx/fresana.html", @"http://www.lonchicuates.com.mx/futbolillo.html", @" ", @"http://www.lonchicuates.com.mx/la-limantour.html", @"http://www.lonchicuates.com.mx/las-chaparritas.html", @"http://www.lonchicuates.com.mx/las-ji-karlitas.html", @"http://www.lonchicuates.com.mx/los-brodys.html", @"http://www.lonchicuates.com.mx/los-caca-cuates.html", @"http://www.lonchicuates.com.mx/los-cantildeeros.html", @"http://www.lonchicuates.com.mx/los-charroles.html", @"http://www.lonchicuates.com.mx/los-travi-esos.html", @"http://www.lonchicuates.com.mx/mangalaacuten.html", @"http://www.lonchicuates.com.mx/manolo-manchego.html", @"http://www.lonchicuates.com.mx/man-sanito.html", @"http://www.lonchicuates.com.mx/mermeladoacuten.html", @"http://www.lonchicuates.com.mx/musculeche.html", @"http://www.lonchicuates.com.mx/panikines.html", @"http://www.lonchicuates.com.mx/pepe-pino.html", @"http://www.lonchicuates.com.mx/pera-loca.html", @"http://www.lonchicuates.com.mx/pollo-neto.html", @"http://www.lonchicuates.com.mx/los-ponchix.html", @"http://www.lonchicuates.com.mx/prisca.html", @"http://www.lonchicuates.com.mx/tae-kwon-rroz.html", @"http://www.lonchicuates.com.mx/velochuga.html", @"http://www.lonchicuates.com.mx/zana-zana.html", nil];
    self.lonchiCuates = cuates;
    
    NSArray *atl = [[NSArray alloc]initWithObjects:@"http://www.lonchicuates.com.mx/erwin-gonzalez.html", @"http://www.lonchicuates.com.mx/nuria-diosdado.html", @"http://www.lonchicuates.com.mx/nataly-michel-de-la-luz.html", @"http://www.lonchicuates.com.mx/jorge-escamilla.html", @"http://www.lonchicuates.com.mx/juan-carlos-cabrera.html", @"http://www.lonchicuates.com.mx/rut-castillo.html", @"http://www.lonchicuates.com.mx/kevin-escamilla.html",  nil];
    self.lonchiAtletas = atl;
	// Do any additional setup after loading the view.
    NSLog(@"%@", categoria);
    
       if ([self.categoria isEqualToString:@"5"]) {
        
        self.bar.tintColor=[UIColor colorWithRed:((float)162/255.0f) green:((float)197/255.0f) blue:((float)24/255.0f) alpha:1];
                 self.navTittle.text = @"Lonchi Cuates";
    }
    if ([self.categoria isEqualToString:@"6"]) {
        
        self.bar.tintColor=[UIColor colorWithRed:((float)208/255.0f) green:((float)200/255.0f) blue:((float)39/255.0f) alpha:1];
                 self.navTittle.text = @"Lonchi Atletas";
    }
    
    UIImage *menuImg=[UIImage imageNamed:@"Botón-regresarNew.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [bar pushNavigationItem:navItem animated:NO];
    
    /////
    
    //// Primer Scroll
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(6, 50, 768, 1024)];
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(6, 50, 320, 480)];
}
    [self.view addSubview: self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake(320, 600);
    [self.scrollView setContentSize:scrollViewContentSize];
    scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = NO;
    scrollView.alpha = 1;
    scrollView.backgroundColor = nil;
    
    scrollView.scrollsToTop = YES;
    
    NSUInteger nimages = 1;
    CGFloat cx = 0;
    CGFloat cy = 0;
    for (;nimages<15 ; nimages++) {
        int drawCount = 0;
        for (int k = 0 + 3*(nimages-1); k < 3*(nimages); k ++) {
            
                       
          if ([self.categoria isEqualToString:@"5"]) {
               NSString *imageNa = [NSString stringWithFormat:@"lonchicuate-%i.png", (k+1)];
              self.imageName =imageNa;
              lonchiCOunt =1;

          }
            else if ([self.categoria isEqualToString:@"6"]) {
                NSString *imageNa = [NSString stringWithFormat:@"lonchiatleta-%i.png", (k+1)];
                self.imageName =imageNa;
                              lonchiCOunt = 2;
            }

        //  else{
          //  NSString *imageNa = [NSString stringWithFormat:@"principal-receta-%i.png", (k+1)];
            //  self.imageName =imageNa;
          //}
            
            UIImage *image = [UIImage imageNamed:self.imageName];
            
            if (image == nil) {
                break;
            }
            
            
            
            
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
          //  self.displayImage = imageView;
            
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                CGRect rect = imageView.frame;
                rect.size.height = 250;
                rect.size.width = 250;
                rect.origin.x = ((scrollView.frame.size.width - (image.size.width))*(drawCount)/2) +cx;
                
                rect.origin.y = 250*(nimages-1) + cy;
                imageView.frame = rect;
                
            }
            
            
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                CGRect rect = imageView.frame;
                rect.size.height = 100;
                rect.size.width = 100;
                rect.origin.x = ((scrollView.frame.size.width - (image.size.width))*(drawCount)) +cx;
                
                rect.origin.y = 100*(nimages-1) + cy;
                imageView.frame = rect;
                
            }

            
            //   NSLog(@"%i, %f %f", drawCount, cx, imageView.frame.origin.x);
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-1.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-1.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack1:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-2.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-2.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack2:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-3.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-3.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack3:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-4.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-4.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack4:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-5.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-5.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack5:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-6.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-6.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack6:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-7.png"]]||[imageName isEqualToString:[NSString stringWithFormat:@"lonchiatleta-7.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack7:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-8.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack8:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-9.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack9:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }

            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-10.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack10:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-11.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack11:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-12.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack12:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-13.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack13:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-14.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack14:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-15.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack15:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-16.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack16:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-17.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack17:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-18.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack18:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-19.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack19:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-20.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack20:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-21.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack21:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-22.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack22:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-23.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack23:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-24.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack24:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-25.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack25:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-26.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack26:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-27.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack27:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-28.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack28:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
           
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-29.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack29:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-30.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack30:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-31.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack31:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-32.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack32:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-33.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack33:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-34.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack34:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-35.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack35:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }
            
            if ([imageName isEqualToString:[NSString stringWithFormat:@"lonchicuate-36.png"]]) {
                imageView.userInteractionEnabled = YES;
                
                UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideBack36:)];
                recognizer.numberOfTapsRequired = 1;
                recognizer.numberOfTouchesRequired = 1;
                
                [imageView addGestureRecognizer:recognizer];   }



            ///////////
            
            [scrollView addSubview:imageView];
            // [imageView release];
            
            drawCount = (drawCount+1)%4;
            
            if (drawCount < 3) {
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                    cx += scrollView.frame.size.width/30;
                }
                if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                    cx += scrollView.frame.size.width/3.3;
                }

            } else{
                cx = 0;
            }
            
            
            
            
        }
        
        cy += scrollView.frame.size.height/80;
        
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        if ([self.categoria isEqualToString:@"6"]) {
            
            [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+3*250)];
        }
        if ([self.categoria isEqualToString:@"5"]) {
            
            [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+13*250)];
        }else{
            
            [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+15*250)];
        }

    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        if ([self.categoria isEqualToString:@"6"]) {
            
            [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+3*100)];
        }
        if ([self.categoria isEqualToString:@"5"]) {
            
            [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+13*100)];
        }else{
            
            [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+15*100)];
        }

    }

    
}

-(IBAction)click
{
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
    
        self.scrollView.alpha = 0;
    
        [UIView commitAnimations];
    
    
  //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
    
}
-(IBAction)click2
{
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
    
        self.scrollView.alpha = 0;
        
        [UIView commitAnimations];
        
        
   // }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
    
}


-(IBAction)slideBack:(id)sender{
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UIViewController *newTopViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
        
        
    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            
            UIViewController *newTopViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
        }
        else if (iOSDeviceScreenSize.height == 568){
            
            UIViewController *newTopViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone5" bundle:nil];
            MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
            
        }
    }
    

}
-(IBAction)slideBack1:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    
    P1.categoriaCOunt = self.categoria;
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:0]];
        
        P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:0]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}


-(IBAction)slideBack2:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
     WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
P1.categoriaCOunt = self.categoria;
   // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
       NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:1]];
    
      P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
       NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:1]];
        
        P1.url = recST;
    }

      newTopViewController = P1;
    
     CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
      
}
-(IBAction)slideBack3:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:2]];
        
        P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:2]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack4:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:3]];
        
        P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:3]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack5:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:4]];
        
        P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:4]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack6:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:5]];
        
        P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:5]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack7:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:6]];
        
        P1.url = recST;
    }
    else if (lonchiCOunt ==2) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiAtletas objectAtIndex:6]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}

-(IBAction)slideBack8:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:7]];
        
        P1.url = recST;
    }
       
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack9:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:8]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack10:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:9]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack11:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:10]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack12:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:11]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack13:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:12]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack14:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:13]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack15:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:14]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack16:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:15]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack17:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:16]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack18:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:17]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack19:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:18]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack20:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:19]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack21:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:20]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack22:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:21]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack23:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:22]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack24:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:23]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack25:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:24]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack26:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:25]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack27:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:26]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack28:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:27]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack29:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:28]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack30:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:29]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack31:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:30]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack32:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    P1.categoriaCOunt = self.categoria;
    
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:31]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack33:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:32]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack34:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:33]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack35:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:34]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}
-(IBAction)slideBack36:(id)sender{
    
    UIViewController *newTopViewController;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    WEBmasViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"WEBmas"];
    
    P1.categoriaCOunt = self.categoria;
    // if ([imageName isEqualToString:@"principal-receta-1.png"]) {
    if (lonchiCOunt ==1) {
        
        NSString * recST  = [NSString stringWithFormat:@"%@",[self.lonchiCuates objectAtIndex:35]];
        
        P1.url = recST;
    }
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
}









- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
