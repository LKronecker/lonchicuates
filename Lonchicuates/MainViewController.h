//
//  MainViewController.h
//  Lonchicuates
//
//  Created by Alan MB on 12/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UIScrollViewDelegate>{
    bool viewCOnt ;
    
    BOOL slideControl;
    int socialBool;
    int ayudaCount;
    
    int recetaCOunt;
    int categoriaCount;
    
    UIImageView *recetaPicker;
    UIView *recetaView;
    NSTimer *timer;
    NSTimer *timerSlide;
    UIButton *recetaButon;
    
    NSArray *recetasAray;
    
    UINavigationBar *bar;
    UIButton *botonMenu;
    UIButton *botonMenu2;
    
    UIImageView * ayudaImage;
     UIImageView * CUATE1;
     UIImageView * CUATE2;
     UIImageView * CUATE3;
    NSArray *recetasINDEX;
    UIButton *botonCERRAR;
    UIScrollView *scrollView;
    
    UIImageView * flechaImage;
    NSTimer *flechaTimer;
    
}

@property (nonatomic, retain) IBOutlet     UIImageView *recetaPicker;
@property (nonatomic, retain) IBOutlet  UIView *recetaView;
 @property (nonatomic, retain) IBOutlet   UIButton *recetaButon;
@property (nonatomic, retain)  NSTimer *timer;
@property (nonatomic, retain)  NSTimer *timerSlide;
  @property (nonatomic, retain)   NSArray *recetasAray;
@property (nonatomic, retain)   NSArray *recetasINDEX;
 @property (nonatomic, retain)   UIScrollView *scrollView;

@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
@property(nonatomic,strong)IBOutlet UIButton *botonCERRAR;


  

@property(strong, nonatomic) IBOutlet UINavigationBar *bar;

@property(strong, nonatomic) IBOutlet UIImageView * CUATE1;
@property(strong, nonatomic) IBOutlet UIImageView * CUATE2;
@property(strong, nonatomic) IBOutlet UIImageView * CUATE3;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu2;
@property(nonatomic,strong)IBOutlet  UIImageView * ayudaImage;


@property(strong, nonatomic) IBOutlet UIImageView * flechaImage;
@property (nonatomic, retain)  NSTimer *flechaTimer;

-(IBAction)click;
-(IBAction)pushReceta1;

-(IBAction)pushMasFuerte;
-(IBAction)pushMasListo;
-(IBAction)pushMasEnergia;
-(IBAction)pushMasSano;
-(IBAction)Lonchicuates;
-(IBAction)Lonchiatletas;

-(IBAction)slideBack:(id)sender;
-(IBAction)slideBack2:(id)sender;
-(IBAction)ayuda;
-(IBAction)ayuda2;
-(IBAction)FaceBook;
-(IBAction)Twitter;
-(IBAction)YouTube;
-(IBAction)Mas;
- (IBAction)tapReceta:(id)sender;
-(IBAction)pushMail;
-(IBAction)TodasRecetas;
-(IBAction)ayudaCERRAR;

- (IBAction)BegginAnimaconFlecha:(id)sender;
- (IBAction)EndAnimaconFlecha:(id)sender;
@end
