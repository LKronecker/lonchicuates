//
//  DetailSocialViewController.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-08-27.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailSocialViewController : UIViewController<UIWebViewDelegate>{
    
    bool orientseason;
    UIWebView *socialWeb;
}
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic)  UIWebView *detailSocialWebOutlet;
@property (retain, nonatomic)  UIWebView *socialWeb;
@property (weak, nonatomic) IBOutlet UIImageView *headOutlet;

@property  (strong,nonatomic) NSString *url;
- (IBAction)exitAction:(id)sender;
@end
