//
//  RecetasViewController.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-08-26.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "RecetasViewController.h"
#import "ECSlidingViewController.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h> 
#import <Accounts/Accounts.h>
#import "TodasRecetasViewController.h"
#import "MainViewController.h"
#import "MasViewController.h"
#import "Ingrediente-RecetaView.h"
#import "AppDelegate.h"
#import "MenuViewController.h"


@interface RecetasViewController ()

@end

@implementation RecetasViewController
@synthesize bar, botonMenu, scrollView, faceBut, twitBut, receta, imagenReceta, timer, titulosRecetas, navTittle, recetasArray, scrollIngredients, botonMenu2, ayudaImage, CUATE1, CUATE3, CUATE2, ImageNAME, botonCERRAR, recTittle, flechaImage, flechaTimer, nutrientesArray, NUTImage, mailBut, urlArray, recoNUT;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    //     self.opcionesView.alpha = 0;
    //    self.recetaView.alpha = 0;
    self.scrollIngredients.alpha =1;
    self.scrollView.alpha =1;
    self.faceBut.alpha = 1;
    self.twitBut.alpha  = 1;
    self.imagenReceta.alpha = 1;
    self.mailBut.alpha = 1;
    
    [UIView commitAnimations];

    
   // imagenReceta.image = [UIImage imageNamed:[NSString stringWithFormat:@"imagen-receta-%@.png", receta]];
    
    

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [self.slidingViewController resetTopView];
     self.botonCERRAR.alpha = 0;
    ayudaCount = 0;
    self.ayudaImage.alpha=0;
    self.CUATE1.alpha = 0;
    self.CUATE2.alpha = 0;
    self.CUATE3.alpha = 0;
    
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
          imagenReceta.image = [UIImage imageNamed:[NSString stringWithFormat:@"imagen-receta-%@-ipad.jpg", receta]];
     }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            imagenReceta.image = [UIImage imageNamed:[NSString stringWithFormat:@"imagen-receta-%@.png", receta]];
        }else if (iOSDeviceScreenSize.height == 568){
            
            imagenReceta.image = [UIImage imageNamed:[NSString stringWithFormat:@"imagen-receta-%@-iphone5.png", receta]];
        }
    }

    
    if([receta isEqualToString:@"1"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"43", @"99", @"50", @"52", @"60", @"59", @"24", @"3", nil];
        self.recetasArray = receArray;
    }
    
    if([receta isEqualToString:@"2"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"113", @"63", @"6", @"22", @"116", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"3"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"47", @"12", @"18", @"62", @"65", @"117", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"4"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"50", @"31", @"47", @"35", @"25", @"3",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"5"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"118", @"99", @"35", @"79", @"113", @"45", @"3",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"6"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"118", @"120", @"73", @"24", @"96", @"109", @"121",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"7"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"32", @"114", @"122", @"3",   nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"8"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"97", @"31", @"27", @"42", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"9"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"11", @"102", @"4", @"100", @"107", @"40", @"116", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"10"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"109", @"96", @"34", @"123", @"116", nil];
        self.recetasArray = receArray;
    }
   ////////////
    if([receta isEqualToString:@"11"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"109", @"124", @"96", @"76", @"61", @"125", nil];
        self.recetasArray = receArray;
    }
    
    if([receta isEqualToString:@"12"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"99", @"4", @"126", @"116", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"13"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"31", @"33", @"99", @"109", @"83", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"14"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"29", @"67", @"90", @"24", @"116",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"15"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"41", @"53", @"127", @"128", @"52", @"24", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"16"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"101", @"44", @"80", @"68", @"3",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"17"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"13", @"34", @"96", @"117", @"88", @"91", @"74", @"116",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"18"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"37", @"14", @"97", @"65", @"45", @"69", @"102", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"19"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"93", @"96", @"23", @"57", @"87", @"50", @"59", @"2", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"20"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"4", @"128", @"108", @"75", @"3",  nil];
        self.recetasArray = receArray;
    }
    ///////
    //////
    if([receta isEqualToString:@"21"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"23", @"96", @"109", @"46", @"70", @"55", @"3", nil];
        self.recetasArray = receArray;
    }
    
    if([receta isEqualToString:@"22"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"106", @"34", @"99", @"114", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"23"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"127", @"95", @"54", @"12", @"109", @"86", @"3",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"24"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"38", @"103", @"0", @"30", @"128", @"52", @"59",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"25"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"92", @"26", @"8", @"15", @"56",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"26"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"31", @"21", @"98", @"109", @"3", @"12", @"59", @"102", @"89",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"27"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"51", @"87", @"99", @"0", @"129", @"102", @"89", @"117", @"65", @"96", @"3",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"28"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"99", @"112", @"39", @"16", @"3",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"29"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"117", @"90", @"65", @"66", @"79", @"77", @"112", @"80", @"78", @"113", @"69", @"74", @"29", @"3",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"30"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"80", @"64", @"71", @"4", @"49", @"3", @"94", @"79", @"48", @"12",  nil];
        self.recetasArray = receArray;
    }
    ///////
    ///////
    if([receta isEqualToString:@"31"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"7", @"9", @"24", @"29", @"3", @"99", @"46", @"70", @"59",nil];
        self.recetasArray = receArray;
    }
    
    if([receta isEqualToString:@"32"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"85", @"20", @"5", @"34", @"130", @"58", @"28", @"4", @"127", @"3", @"131", @"12",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"33"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"82", @"4", @"105", @"59", @"90", @"85", @"111", @"81", @"89", @"1", @"102", @"3",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"34"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"10", @"4", @"104", @"107", @"87", @"59", @"95", @"60", @"3",  nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"35"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"47", @"12", @"80", @"63", @"69", @"74", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"36"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"79", @"19", @"12", @"98", @"49", @"41", @"3",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"37"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"33", @"115", @"72", @"36", @"78", @"113", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"38"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"34", @"109", @"101", @"4", @"102", @"89", @"73", @"3", nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"39"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"20", @"31", @"98", @"132", @"4", @"65", @"3",nil];
        self.recetasArray = receArray;
    }
    if([receta isEqualToString:@"40"]){
        NSArray *receArray =[[NSArray alloc]initWithObjects:@"37", @"84", @"98", @"1", @"17", @"61", @"79", @"14", @"116",  nil];
        self.recetasArray = receArray;
    }
    ////////////
    

    NSArray *urlA =[[NSArray alloc]initWithObjects: @"http://goo.gl/S2javg", @"http://goo.gl/VnUlpX", @"http://goo.gl/MnmWgG", @"http://goo.gl/IPSX10", @"http://goo.gl/J84HJ2", @"http://goo.gl/njVdcc", @"http://goo.gl/6HQWR2", @"http://goo.gl/kcwh8R", @"http://goo.gl/ZvwTWi", @"http://goo.gl/6mBbsq", @"http://goo.gl/d5iPAg", @"http://goo.gl/D9Sz6d", @"http://goo.gl/Y7VOp4", @"http://goo.gl/LGioc9", @"http://goo.gl/7IDHBo", @"http://goo.gl/u9CS3f", @"http://goo.gl/SgFcnN", @"http://goo.gl/DJGmnS", @"http://goo.gl/F2LXHv", @"http://goo.gl/ox14ks", @"http://goo.gl/qiGu4Q", @"http://goo.gl/YRSRjf", @"http://goo.gl/lfuEQM", @"http://goo.gl/fq9zwb", @"http://goo.gl/prk6LC", @"http://goo.gl/eQAQcv", @"http://goo.gl/nXiuYK", @"http://goo.gl/PxiALG", @"http://goo.gl/7B7mkW", @"http://goo.gl/kNQXT2", @"http://goo.gl/Fd9kBn", @"http://goo.gl/3bnSp8", @"http://goo.gl/1Q6fl8", @"http://goo.gl/Bca8BA", @"http://goo.gl/iYaH2G", @"http://goo.gl/cZdsXm", @"http://goo.gl/tUJoJp", @"http://goo.gl/ixnQYb", @"http://goo.gl/OwWeFz", @"http://goo.gl/vDvfHx",nil];
    self.urlArray =urlA;
    
    
    NSArray *nurarray = [[NSArray alloc]initWithObjects:@"DN1", @"DN2", @"DN3", @"DN4", @"DN5", @"DN6", @"DN7", @"DN8", @"DN9", @"DN10", @"DN11", @"DN12", @"DN13", @"DN14", @"DN15", @"DN16", @"DN17", @"DN18", @"DN19", @"DN20",  @"DN21",  @"DN22",  @"DN23",  @"DN24",  @"DN25",  @"DN26",  @"DN27",  @"DN28",  @"DN29",  @"DN30",@"DN31", @"DN32", @"DN33", @"DN34", @"DN35", @"DN36", @"DN37", @"DN38", @"DN39", @"DN40", nil];
    
    self.nutrientesArray = nurarray;
    
    NSArray *array = [[NSArray alloc]initWithObjects:  @"La vitamina que C necesita", @"A mover el esqueleto", @"Dale a tu cuerpo más energía", @"Ánimo para campeonas y campeones", @"Tus huaraches con hierro", @"Ahí viene la A", @"Para que desquites, tus esquites", @"Fibra para la panza", @"Como los japonesitos", @"Para aplicarse, un burrito", @"Don Pepino va a la escuela", @"Ponte fresa", @"Las espinacas se llevan con las fresas", @"¿Me das una mordidta?", @"La defensiva contra las infecciones", @"Bueno para los nervios", @"¡Mmmh! Que rico mi mollete", @"Para crecer en grande", @"Mamma mía ¡Que italiano!", @"Tún, tún, sushi con atún", @"¿Qué hongo mi champiñón?", @"¡Muy mexicanos y muy nutritivos!", @"Sana sana, la manzana", @"De todo un poco", @"Te presentamos a la Tía Mina", @"Trenecito verde", @"Lancitas africanas", @"Solecito verde", @"Frutiflechas", @"Caracoches", @"Espiroapios", @"Las prietas están tostaditas", @"Papa ratón", @"El tricolor", @"El francesito volador", @"El bananas", @"Witzi Witzi la morita", @"Golazo frijolero", @"Quesabrosillas", @"Pastamar", nil];
    
    self.titulosRecetas = array;
    
    self.recTittle.text = [NSString stringWithFormat:@"%@", [self.titulosRecetas objectAtIndex:[self.receta intValue]-1]];
    
    
    int r = [self.receta integerValue]-1;
    
    NSString *titulo =[NSString stringWithFormat:@"Receta %@", self.receta];
    self.bar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];
    self.navTittle.text = titulo;
    
    NSLog(@"%@, %i", titulo, r);
    
    
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Header-640-x-88.png"] drawInRect:(CGRectMake(0, 0, 320, 44))];
  //  UIImage *imageH = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
  //  [self.bar setBackgroundImage:imageH forBarMetrics:UIBarMetricsDefault];
    ////
   //  self.bar.backgroundColor=[UIColor colorWithRed:((float)44/255.0f) green:((float)87/255.0f) blue:((float)138/255.0f) alpha:1];
    /////
 /*   UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake ( 5, 10, 29, 25);
    //[button setTitle:@"Regresar" forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"Bot¢n-header.png"] forState:UIControlStateNormal];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithCustomView:button] ;
    self.navigationItem.leftBarButtonItem = backButton;
  */
    ////
    
    

    UIImage *menuImg=[UIImage imageNamed:@"Botón-regresarNew.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [bar pushNavigationItem:navItem animated:NO];
    ////
    UIImage *menuImg2=[UIImage imageNamed:@"Botton-ayuda.png"];
    self.botonMenu2 =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu2.frame=CGRectMake(279, 8, 40, 35);
    [self.botonMenu2 setBackgroundImage:menuImg2 forState:UIControlStateNormal];
    [self.botonMenu2  addTarget:self action:@selector(ayuda) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem2= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu2];
    //  UINavigationItem *navItem2=[[UINavigationItem alloc] init];
    navItem.rightBarButtonItem=menuItem2;
    [ self.navigationController.navigationBar addSubview:botonMenu2];
    /////

    //// SLIDE
    
 //   if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {

   ///////
   // }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        CGRect scrollViewFrame = CGRectMake(0, 667, 768, 229);
        self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
        CGRect scrollViewFrame = CGRectMake(2, 315, 320, 105);
        self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
            
        }else if (iOSDeviceScreenSize.height == 568){

            CGRect scrollViewFrame = CGRectMake(2, 350, 360, 150);
            self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
        }

    }
    
    [self.view addSubview: self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake(600, 600);
    [self.scrollView setContentSize:scrollViewContentSize];
    scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = YES;
    scrollView.alpha = 1;
    scrollView.backgroundColor = nil;
    
    NSUInteger nimages = 1;
    CGFloat cx = 0;
    for (; ; nimages++) {
        // NSString *imageName = [NSString stringWithFormat:@"%d.png", (nimages)];
        
        //  UIImage *image = [UIImage imageNamed:imageName];
        //UIImageView *imageView = [[UIImageView alloc] init];
        
        //  [self downloadImage:imageName inBackground:imageView];
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            NSString *imageName = [NSString stringWithFormat:@"Slide-receta-%@-%d-ipad.png", receta, (nimages)];
            self.ImageNAME = imageName;
            
            UIImage *image = [UIImage imageNamed:self.ImageNAME];
            
            if (image == nil) {
                break;
            }
            UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
            
            
            
            CGRect rect = imageView.frame;
            rect.size.height = 250;
            rect.size.width = 768;
            rect.origin.x = ((scrollView.frame.size.width - (image.size.width))/51) + cx+20;
            rect.origin.y = -5;
            
            imageView.frame = rect;
            
            [scrollView addSubview:imageView];

        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){

        NSString *imageName = [NSString stringWithFormat:@"Slide-receta%@-%d.png", receta, (nimages)];
                self.ImageNAME = imageName;
                
                UIImage *image = [UIImage imageNamed:self.ImageNAME];
                
                if (image == nil) {
                    break;
                }
                UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                
                
                
                CGRect rect = imageView.frame;
                rect.size.height = 105;
                rect.size.width = 320;
                rect.origin.x = ((scrollView.frame.size.width - (image.size.width))/51) + cx;
                rect.origin.y = -5;
                
                imageView.frame = rect;
                
                [scrollView addSubview:imageView];

            }else if (iOSDeviceScreenSize.height == 568){
                
                NSString *imageName = [NSString stringWithFormat:@"Slide-receta%@-%d-iphone5.png", receta, (nimages)];
                self.ImageNAME = imageName;
                
                UIImage *image = [UIImage imageNamed:self.ImageNAME];
                
                if (image == nil) {
                    break;
                }
                UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                
                
                
                CGRect rect = imageView.frame;
                rect.size.height = 185;
                rect.size.width = 320;
                rect.origin.x = ((scrollView.frame.size.width - (image.size.width))/51) + cx;
                rect.origin.y = -5;
                
                imageView.frame = rect;
                
                [scrollView addSubview:imageView];

                
            }
        }
        
               cx += scrollView.frame.size.width;
        
    }
    
    
    [scrollView setContentSize:CGSizeMake(cx, [scrollView bounds].size.height)];
   // [self.view addSubview:scrollView];
  ///////
    ///////
    ///////
    /////
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        CGRect scrollViewFrameIngre = CGRectMake(-40, 430, 800, 210);
        
        self.scrollIngredients = [[UIScrollView alloc] initWithFrame:scrollViewFrameIngre];
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            CGRect scrollViewFrameIngre = CGRectMake(0, 204, 320, 105);
            
            self.scrollIngredients = [[UIScrollView alloc] initWithFrame:scrollViewFrameIngre];

        }
        
    else if (iOSDeviceScreenSize.height == 568){    
    CGRect scrollViewFrameIngre = CGRectMake(0, 234, 320, 105);
        // CGRect scrollViewFrameIngre = CGRectMake(0, 234, 320, 105);
    
    self.scrollIngredients = [[UIScrollView alloc] initWithFrame:scrollViewFrameIngre];
    }
    }
    
    [self.view addSubview: self.scrollIngredients];
    CGSize scrollViewContentSizeIng = CGSizeMake(600, 600);
    [self.scrollIngredients setContentSize:scrollViewContentSizeIng];
    scrollIngredients.delegate = self;
    
    [self.scrollIngredients setBackgroundColor:[UIColor blackColor]];
    [scrollIngredients setCanCancelContentTouches:NO];
    
    scrollIngredients.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollIngredients.clipsToBounds = YES;
    scrollIngredients.scrollEnabled = YES;
    scrollIngredients.pagingEnabled = NO;
    scrollIngredients.alpha = 1;
    scrollIngredients.backgroundColor = nil;
    
    NSUInteger nimagesIng = 1;
    CGFloat cxI = 0;
    for (;nimagesIng<[self.recetasArray count] ; nimagesIng++) {
        // NSString *imageName = [NSString stringWithFormat:@"%d.png", (nimages)];
        
        //  UIImage *image = [UIImage imageNamed:imageName];
        //UIImageView *imageView = [[UIImageView alloc] init];
        
        //  [self downloadImage:imageName inBackground:imageView];
        
        NSString *imageName = [NSString stringWithFormat:@"%@.png", [self.recetasArray objectAtIndex:nimagesIng]];
        //   NSString *imageName = [NSString stringWithFormat:@"fondoWeb.png"];
        
        UIImage *image = [UIImage imageNamed:imageName];
        
        if (image == nil) {
            break;
        }
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.userInteractionEnabled = YES;
        
        if ([imageName isEqualToString:[NSString stringWithFormat:@"0.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap0:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
            self.Ingre1 = imageView;
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"1.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap1:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
            self.Ingre2 = imageView;
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"2.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap2:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
            self.Ingre3 = imageView;
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"3.png"]]) {
             imageView.userInteractionEnabled = YES;
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap3:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
            self.Ingre4 =imageView;
           
            
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"4.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap4:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"5.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap5:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"6.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap6:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"7.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap7:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"8.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap8:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"9.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap9:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"10.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap10:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"11.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap11:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"12.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap12:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"13.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap13:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"14.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap14:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"15.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap15:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"16.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap16:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"17.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap17:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"18.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap18:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"19.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap19:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"20.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap20:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"21.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap21:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"22.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap22:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"23.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap23:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"24.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap24:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"25.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap25:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"26.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap26:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"27.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap27:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"28.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap28:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"29.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap29:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"30.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap30:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"31.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap31:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"32.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap32:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"33.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap33:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"34.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap34:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"35.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap35:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"36.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap36:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"37.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap37:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"38.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap38:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"39.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap39:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"40.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap40:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"41.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap41:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"42.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap42:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"43.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap43:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"44.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap44:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"45.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap45:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"46.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap46:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"47.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap47:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"48.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap48:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"49.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap49:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"50.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap50:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
            

        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"51.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap51:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"52.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap52:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"53.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap53:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"54.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap54:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"55.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap55:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"56.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap56:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"57.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap57:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"58.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap58:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"59.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap59:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"60.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap60:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"61.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap61:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"62.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap62:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"63.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap63:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"64.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap64:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"65.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap65:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"66.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap66:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"67.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap67:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"68.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap68:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"69.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap69:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"70.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap70:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"71.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap71:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"72.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap72:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"73.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap73:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"74.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap74:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"75.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap75:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"76.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap76:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"77.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap77:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"78.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap78:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"79.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap79:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"80.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap80:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"81.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap81:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"82.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap82:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"83.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap83:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"84.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap84:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"85.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap85:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"86.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap86:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"87.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap87:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"88.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap88:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"89.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap89:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"90.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap90:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"91.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap91:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"92.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap92:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"93.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap93:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"94.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap94:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"95.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap95:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"96.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap96:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"97.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap97:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"98.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap98:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"99.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap99:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"100.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap100:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"101.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap101:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"102.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap102:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"103.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap103:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"104.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap104:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"105.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap105:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"106.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap106:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"107.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap107:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"108.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap108:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"109.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap109:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"110.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap110:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"111.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap111:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"112.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap112:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"113.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap113:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"114.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap114:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"115.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap115:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"116.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap116:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"117.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap117:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"118.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap118:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"119.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap119:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"120.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap120:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"121.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap121:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"122.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap122:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"123.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap123:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"124.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap124:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"125.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap125:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"126.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap126:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"127.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap127:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"128.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap128:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"129.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap129:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        if ([imageName isEqualToString:[NSString stringWithFormat:@"130.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap130:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        
        if ([imageName isEqualToString:[NSString stringWithFormat:@"131.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap131:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        
        if ([imageName isEqualToString:[NSString stringWithFormat:@"132.png"]]) {
            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingTap132:)];
            recognizer.numberOfTapsRequired = 1;
            recognizer.numberOfTouchesRequired = 1;
            [imageView addGestureRecognizer:recognizer];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            CGRect rect = imageView.frame;
            rect.size.height = 154;
            rect.size.width = 103;
            rect.origin.x = ((scrollIngredients.frame.size.width - (image.size.width))/13) + cxI;
            rect.origin.y = 0;
            
            imageView.frame = rect;
        }
        
         if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        CGRect rect = imageView.frame;
        rect.size.height = 82;
        rect.size.width = 50;
        rect.origin.x = ((scrollIngredients.frame.size.width - (image.size.width))/13) + cxI;
        rect.origin.y = 0;
        
        imageView.frame = rect;
         }
        
        [scrollIngredients addSubview:imageView];
        // [imageView release];
        
      //  cxI += scrollIngredients.frame.size.width/4;
        cxI += scrollIngredients.frame.size.width/6;
    }
    
    
    [scrollIngredients setContentSize:CGSizeMake(cxI+30, [scrollIngredients bounds].size.height)];
    
///////
    [self.view bringSubviewToFront:faceBut];
        [self.view bringSubviewToFront:twitBut];
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ayuda2)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    self.recoNUT =recognizer;
    
    self.ayudaImage.userInteractionEnabled = YES;
    [self.ayudaImage addGestureRecognizer:recognizer];
    
    self.flechaImage.alpha = 1;
    [self.view bringSubviewToFront:self.flechaImage];
    
    flechaTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(EndAnimaconFlecha:) userInfo:nil repeats:NO];
    
    self.NUTImage.alpha =0;

   
    
   
        UITapGestureRecognizer *recognizerNUT = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(EndAnimaconNutrientes:)];
        recognizerNUT.numberOfTapsRequired = 1;
        recognizerNUT.numberOfTouchesRequired = 1;
    
        [self.NUTImage addGestureRecognizer:recognizerNUT];
     self.NUTImage.userInteractionEnabled = YES;
    
    nutCOunt = 0;
       // self.Ingre1 = imageView;

    
    // [self.view bringSubviewToFront:recetaPicker];
}
- (IBAction)BegginAnimaconFlecha:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.flechaImage.alpha = 1;
    
    [UIView commitAnimations];
    flechaTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(EndAnimaconFlecha:) userInfo:nil repeats:NO];


}
- (IBAction)EndAnimaconFlecha:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.flechaImage.alpha = 0;
    
    [UIView commitAnimations];
    
    flechaTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(BegginAnimaconFlecha:) userInfo:nil repeats:NO];


}

- (IBAction)BegginAnimaconNutrientes:(id)sender{
    if (nutCOunt == 0) {
        self.view.opaque = YES;
        nutCOunt =1;
        [self.ayudaImage removeGestureRecognizer:recoNUT];
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.NUTImage.alpha = 1;
    self.botonCERRAR.alpha = 1;
        self.ayudaImage.alpha = 1;
         self.ayudaImage.image = [UIImage imageNamed:@"fondo-datos-nutricionales.png"];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        NSString *nut =[NSString stringWithFormat:@"%@-ipad", [self.nutrientesArray objectAtIndex:[self.receta intValue]-1]];
        NUTImage.image = [UIImage imageNamed:nut];
    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            NSString *nut =[NSString stringWithFormat:@"%@", [self.nutrientesArray objectAtIndex:[self.receta intValue]-1]];
            NUTImage.image = [UIImage imageNamed:nut];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            NSString *nut =[NSString stringWithFormat:@"%@", [self.nutrientesArray objectAtIndex:[self.receta intValue]-1]];
            NUTImage.image = [UIImage imageNamed:nut];
        }
    }

    
    [UIView commitAnimations];
        
           [self.view bringSubviewToFront:ayudaImage];
    
    [self.view bringSubviewToFront:NUTImage];
    
    [self.view bringSubviewToFront:botonCERRAR];
    
    }else{
        nutCOunt = 0;
    
        self.botonCERRAR.alpha = 0;
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.NUTImage.alpha =0;
       
        [UIView commitAnimations];
        

    }

}
- (IBAction)EndAnimaconNutrientes:(id)sender{
    nutCOunt = 0;
    
    self.botonCERRAR.alpha = 0;
    [self.ayudaImage addGestureRecognizer:recoNUT];
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.NUTImage.alpha =0;
            self.ayudaImage.alpha = 0;
    
    [UIView commitAnimations];
}

-(IBAction)tapIngredient{
    
   // if([self.Ingre4.image == [UIImage imageNamed:@"4"]])
}

-(IBAction)ayuda{
    
    [self.ayudaImage addGestureRecognizer:recoNUT];
    self.faceBut.alpha = 0;
    self.mailBut.alpha = 0;
    self.twitBut.alpha = 0;
    ayudaCount = 1;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.ayudaImage.alpha = 1;
    self.botonCERRAR.alpha = 1;
   
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        ayudaImage.image = [UIImage imageNamed:@"Ayuda-6-ipad.png"];
    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
             ayudaImage.image = [UIImage imageNamed:@"Ayuda-5-new.png"];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-5-iphone5.png"];
        }
    }

    self.CUATE1.alpha = 1;
    // self.CUATE2.alpha = 1;
    // self.CUATE3.alpha = 1;
    [UIView commitAnimations];
    
    [self.view bringSubviewToFront:ayudaImage];
    
    // [self.view bringSubviewToFront:CUATE2];
    [self.view bringSubviewToFront:CUATE1];
    
    [self.view bringSubviewToFront:botonCERRAR];
    self.navigationController.navigationBarHidden = YES;
   
}
-(IBAction)ayuda2{
    
    if (ayudaCount ==0) {
        ayudaCount = 1;
        self.faceBut.alpha = 0;
        self.mailBut.alpha = 0;
        self.twitBut.alpha = 0;

        self.botonCERRAR.alpha = 1;
        
        self.ayudaImage.alpha = 0;
     //   slideControl = 0;
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-6-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-5-new.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-5-iphone5.png"];
            }
        }

        self.ayudaImage.alpha = 1;
        self.CUATE1.alpha = 1;
        // self.CUATE2.alpha = 1;
        // self.CUATE3.alpha = 1;
        [UIView commitAnimations];
        
        [self.view bringSubviewToFront:ayudaImage];
        //
        [self.view bringSubviewToFront:CUATE1];
        [self.view bringSubviewToFront:botonCERRAR];
        
        self.navigationController.navigationBarHidden = YES;
        
    }
    
    else  if (ayudaCount ==1) {
        ayudaCount = 2;
        
        self.faceBut.alpha = 1;
        self.mailBut.alpha = 1;
        self.twitBut.alpha = 1;

        
        self.ayudaImage.alpha = 0;
      //  slideControl = 0;
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-7-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-6-new.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-6-iphone5.png"];
            }
        }

        self.CUATE1.alpha = 0;
        self.CUATE2.alpha = 1;
        // self.CUATE3.alpha = 0;
        self.ayudaImage.alpha = 1;
        [UIView commitAnimations];
        
        [self.view bringSubviewToFront:CUATE2];
    }
    else if (ayudaCount ==2){
        ayudaCount = 0;
        self.faceBut.alpha = 1;
        self.mailBut.alpha = 1;
        self.twitBut.alpha = 1;

        self.ayudaImage.alpha = 0;
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-8-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-7-new.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-7-iphone5.png"];
            }
        }

        self.CUATE1.alpha = 0;
        self.CUATE2.alpha = 0;
        self.CUATE3.alpha = 1;
        self.ayudaImage.alpha = 1;
        [UIView commitAnimations];
        
        [self.view bringSubviewToFront:CUATE3];
        
        
        
    }
}

-(IBAction)ayudaCERRAR{
        
  //  slideControl = 0;
    self.botonCERRAR.alpha = 0;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.NUTImage.alpha =0;
    self.ayudaImage.alpha = 0;
    self.CUATE1.alpha = 0;
    self.CUATE2.alpha = 0;
    self.CUATE3.alpha = 0;
    self.scrollView.alpha = 1;
    
    self.faceBut.alpha = 1;
    self.mailBut.alpha = 1;
    self.twitBut.alpha = 1;

    [UIView commitAnimations];
    
    
 //   self.opcionesView.userInteractionEnabled = YES;
  //  self.recetaView.userInteractionEnabled = YES;
    
    self.navigationController.navigationBarHidden = NO;
    [ self.navigationController.navigationBar addSubview:botonMenu];
    [ self.navigationController.navigationBar addSubview:botonMenu2];
}


-(IBAction)click
{
     tabBarSelect = 0;
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
   //     self.opcionesView.alpha = 0;
    //    self.recetaView.alpha = 0;
        
        self.scrollView.alpha =0;
        self.faceBut.alpha = 0;
        self.twitBut.alpha  = 0;
        self.imagenReceta.alpha = 0;
    self.scrollIngredients.alpha =0;
    self.mailBut.alpha = 0;
        
        [UIView commitAnimations];
        
        
  //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];

}
-(IBAction)clickTodasRece
{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"TodasRecetas"];
    
    newTopViewController = P1;
    P1.categoria= @"5";
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
    
}


-(IBAction)slideBack:(id)sender{
    
        if ([self.procedence isEqualToString:@"1"]) {
      
             UIViewController *newTopViewController;
       // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"TodasRecetas"];
   
        
      newTopViewController = P1;
      P1.categoria= self.categoST;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
              
       
       }
        /*else if ([self.procedence isEqualToString:@"2"]) {
            
            UIViewController *newTopViewController;
            // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
            
            newTopViewController = P1;
           // P1.categoria= self.categoST;
           // P1.ingrediente =
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
            
         else if ([self.procedence isEqualToString:nil]){
         UIViewController *newTopViewController;
         // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
         MainViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
         
         newTopViewController = P1;
         
         CGRect frame = self.slidingViewController.topViewController.view.frame;
         self.slidingViewController.topViewController = newTopViewController;
         self.slidingViewController.topViewController.view.frame = frame;
         [self.slidingViewController resetTopView];
         
         }

        }*/
    
        else {
            UIViewController *newTopViewController;
            // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            MainViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
            
        }


    
}

-(IBAction)clickMas
{
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    //     self.opcionesView.alpha = 0;
    //    self.recetaView.alpha = 0;
    
    self.scrollView.alpha =0;
    self.faceBut.alpha = 0;
    self.twitBut.alpha  = 0;
    self.imagenReceta.alpha = 0;
    self.mailBut.alpha = 0;
    self.scrollIngredients.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideMas:) userInfo:nil repeats:NO];
    
}

-(IBAction)slideMas:(id)sender{
    
    UIViewController *newTopViewController;
  //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    MasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"MasView"];
   
    
    newTopViewController = P1;
    
    
    
    // [self.slidingViewController resetTopViewWithAnimations:nil onComplete:^{
    //
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    //    self.navigationController.navigationBarHidden = NO;
    NSLog(@"hheeeey");
    //  }];
    
    
    //  [self.slidingViewController resetTopView];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//////SOCIALES

-(IBAction)FBpost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:@" Mira esta rica receta que encontré en la App de los #Lonchicuates"];
        NSString *url = [NSString stringWithFormat:@"%@", [self.urlArray objectAtIndex:[self.receta intValue]-1]];
        //Adding the URL to the facebook post value from iOS
        [controller addURL:[NSURL URLWithString:url]];
        
        //Adding the Text to the facebook post value from iOS
        [controller addImage:self.imagenReceta.image];
        
        
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"App de Lonchicuates" message:@"Configura tus redes sociales." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
       

    }
    
    
}
-(IBAction)TWpost{
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:@" Mira esta rica receta que encontré en la App de los @Lonchicuates"];
        

        [controller addImage:self.imagenReceta.image];
        
        NSString *url = [NSString stringWithFormat:@"%@", [self.urlArray objectAtIndex:[self.receta intValue]-1]];
        [controller addURL:[NSURL URLWithString:url]];

        
        
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"App de Lonchicuates" message:@"Configura tus redes sociales." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
    }
}


-(IBAction)FBwev{
    
    NSURL *webURL = [NSURL URLWithString:@"http://www.facebook.com/Century21Mexico?fref=ts"];
    [[UIApplication sharedApplication] openURL: webURL];
    
}
-(IBAction)TWweb{
    
    NSURL *webURL = [NSURL URLWithString:@"https://twitter.com/c21mexico"];
    [[UIApplication sharedApplication] openURL: webURL];
    
}

////TAB BAr
-(IBAction)buscar{

    [self.slidingViewController anchorTopViewTo:ECRight];
    
    AppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    ECSlidingViewController *rootController = (ECSlidingViewController *)appDelegate.window.rootViewController;
    [(MenuViewController *)rootController.underLeftViewController  activateSearch];

}

- (IBAction)exitAction:(id)sender {
    [self dismissViewControllerAnimated:YES  completion:nil];
    [self.slidingViewController anchorTopViewTo:ECRight];
}
-(IBAction)pushMail{
    NSString *url = [NSString stringWithFormat: @"mailto:contacto@lonchicuates.com?&subject=&body="];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    

}

-(IBAction)postMail{
    
    NSString *urlST = [NSString stringWithFormat:@"%@", [self.urlArray objectAtIndex:[self.receta intValue]-1]];
       NSString *subjST = [NSString stringWithFormat:@"%@", [self.titulosRecetas objectAtIndex:[self.receta intValue]-1]];
    NSString *imgST = [NSString stringWithFormat:@"imagen-receta-%i-iphone5", [self.receta intValue]-1];
    
    
    
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
  //  NSArray *recipients = [[NSArray alloc]initWithObjects:@"", nil];
  //  [picker setToRecipients:recipients];
    [picker setSubject:[NSString stringWithFormat:@"%@", subjST]];
    [picker setMessageBody:[NSString stringWithFormat:@"¡Que bueno que estés a punto de recibir una de nuestras deliciosas recetas!Esperamos que esta receta te guste y la disfrutes con tus hijos.               %@ %@",subjST, urlST] isHTML:YES];
    [picker addAttachmentData:[NSData dataWithContentsOfFile:imgST] mimeType:@"png" fileName:subjST ];
    

    [self presentViewController:picker animated:YES completion:NULL];
    
      
}
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ReceTodas"]) {
        TodasRecetasViewController *destViewController = segue.destinationViewController;
        
    }
}
-(IBAction)ingTap0:(id)sender{
       UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"0"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap1:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"1"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap2:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"2"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap3:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"3"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap4:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"4"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap5:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"5"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap6:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"6"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap7:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"7"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap8:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"8"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap9:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"9"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap10:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"10"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap11:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"11"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap12:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"12"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap13:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"13"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap14:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"14"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap15:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"15"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap16:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    NSString * recST  = [NSString stringWithFormat:@"16"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap17:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"17"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap18:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"18"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap19:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"19"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap20:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"20"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap21:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"21"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap22:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"22"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap23:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"23"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap24:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"24"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap25:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"25"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap26:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"26"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap27:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"27"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap28:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"28"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap29:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"29"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap30:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"31"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap31:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"31"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap32:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"32"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap33:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"33"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap34:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"34"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap35:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"35"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap36:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"36"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap37:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"37"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap38:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"38"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap39:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"39"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap40:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    NSString * recST  = [NSString stringWithFormat:@"40"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap41:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"41"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap42:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"42"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap43:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    NSString * recST  = [NSString stringWithFormat:@"43"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap44:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"44"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap45:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"40"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap46:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"46"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap47:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"47"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap48:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"48"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap49:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"49"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap50:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"50"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap51:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"51"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap52:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"52"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap53:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"53"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap54:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"54"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap55:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"55"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap56:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"56"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap57:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"57"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap58:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"58"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap59:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"59"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap60:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"60"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap61:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"61"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap62:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"62"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap63:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"63"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap64:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"64"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap65:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"65"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap66:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"66"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap67:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"67"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap68:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"68"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap69:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"69"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}

-(IBAction)ingTap70:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"70"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap71:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"71"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap72:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    
    NSString * recST  = [NSString stringWithFormat:@"72"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap73:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"73"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap74:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"74"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap75:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"75"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap76:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"76"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap77:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"77"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap78:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"78"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap79:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"79"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap80:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"80"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap81:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"81"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap82:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"82"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap83:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"83"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap84:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"84"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap85:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"85"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap86:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"86"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap87:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"87"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap88:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"88"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap89:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"89"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap90:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"90"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap91:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"91"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap92:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"92"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap93:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"93"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap94:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"94"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap95:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"95"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap96:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"96"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap97:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"97"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap98:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"98"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap99:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"99"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap100:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"100"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap101:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"101"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap102:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"102"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap103:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"103"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap104:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"104"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap105:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"105"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap106:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"106"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap107:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"107"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap108:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"108"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap109:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"109"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap110:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"110"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap111:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"111"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap112:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"112"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap113:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"113"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap114:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    NSString * recST  = [NSString stringWithFormat:@"114"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap115:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"115"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap116:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"116"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap117:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"117"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap118:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"118"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap119:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"119"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap120:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"120"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap121:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"121"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap122:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"122"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap123:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"123"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap124:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"124"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap125:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"125"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap126:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];
    NSString * recST  = [NSString stringWithFormat:@"126"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap127:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"127"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap128:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"128"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap129:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"129"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap130:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"130"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap131:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"131"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)ingTap132:(id)sender{
    UIViewController *newTopViewController;
    Ingrediente_RecetaView *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Ingrediente_Receta"];

    
    NSString * recST  = [NSString stringWithFormat:@"132"];
    
    P1.ingrediente = recST;
    
    [self dismissViewControllerAnimated:YES  completion:nil];
    
    newTopViewController = P1;
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}







@end
