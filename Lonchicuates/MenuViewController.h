//
//  MenuViewController.h
//  Lonchicuates
//
//  Created by Alan MB on 12/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecetasViewController.h"

@class RecetasViewController;

@interface MenuViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate>{
    RecetasViewController *recetas;
    UISearchBar *menuSearch;
    UISearchDisplayController *searchDisplayController;
     NSMutableArray *searchArray;
    
    UITableView *searchTable;
    UITableView *menuTable;
    NSArray *titulosRecetas;
    NSArray *ingredienteAC;
    NSArray *ingrediente;
    
    int searchCount;
}
 @property (nonatomic, retain)  RecetasViewController *recetas;
  @property (nonatomic, retain) IBOutlet  UITableView *menuTable;
 @property (nonatomic, retain) IBOutlet  UITableView *searchTable;

@property (nonatomic, retain) IBOutlet UISearchBar *menuSearch;
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchDisplayController;
@property (strong, nonatomic) NSMutableArray *searchArray;
@property(nonatomic,retain)IBOutlet  NSArray *titulosRecetas;
@property(nonatomic,retain)IBOutlet NSArray *ingredienteAC;
@property(nonatomic,retain)IBOutlet NSArray *ingrediente;

- (IBAction) tapBackground: (id) sender;
-(void)activateSearch;
@end
