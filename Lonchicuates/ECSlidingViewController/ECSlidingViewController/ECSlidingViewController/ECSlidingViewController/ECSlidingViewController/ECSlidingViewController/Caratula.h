//
//  Caratula.h
//  Lonchicuates
//
//  Created by Alan MB on 13/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Caratula : UIViewController

@property (nonatomic) NSNumber *pageNumber;

-(IBAction)clickMenu;


@end
