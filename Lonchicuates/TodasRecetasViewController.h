//
//  TodasRecetasViewController.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-03.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodasRecetasViewController : UIViewController<UIScrollViewDelegate>{

    UIScrollView *scrollView;
    UIScrollView *scrollView1;
    UIScrollView *scrollView2;
    UIScrollView *scrollView3;
    UINavigationBar *bar;
    UIButton *botonMenu;
    
     UIImageView * CUATE3;
     UIButton *botonMenu2;
    UIImageView * ayudaImage;
    NSTimer *timer;
    UIImageView *displayImage;
    UILabel *navTittle;
    
    NSArray *recetasCategoria;
    UIButton *botonCERRAR;
    int recetaTag;
}

@property (nonatomic, retain)  UIScrollView *scrollView;
@property (nonatomic, retain)  UIScrollView *scrollView1;
@property (nonatomic, retain)  NSTimer *timer;
@property (nonatomic, retain)  UIScrollView *scrollView2;
@property (nonatomic, retain)  UIScrollView *scrollView3;
@property (nonatomic, retain)     NSArray *recetasCategoria;
@property(strong, nonatomic) IBOutlet UINavigationBar *bar;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
 @property (nonatomic, retain) IBOutlet    UILabel *navTittle;

@property(strong, nonatomic) IBOutlet UIImageView * CUATE3;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu2;
@property(nonatomic,strong)IBOutlet  UIImageView * ayudaImage;

 @property (nonatomic, retain)  UIImageView *displayImage;

@property (nonatomic, strong) NSString *categoria;

@property(nonatomic,strong)IBOutlet UIButton *botonCERRAR;



- (IBAction)exitAction:(id)sender ;
-(IBAction)receTap1:(id)sender;
-(IBAction)ayudaCERRAR;

@end
