//
//  WEBmasViewController.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-10.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "WEBmasViewController.h"
#import "ECSlidingViewController.h"
#import "MainViewController.h"
#import "CategoriasViewController.h"
#import "MasViewController.h"

@interface WEBmasViewController ()

@end

@implementation WEBmasViewController
@synthesize bar, botonMenu, socialWeb, timer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.bar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];
    
    UIImage *menuImg=[UIImage imageNamed:@"Botón-regresarNew.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [bar pushNavigationItem:navItem animated:NO];


    UIWebView *web =[[UIWebView alloc]init];
    
    web.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight;
    web.scalesPageToFit=YES;
    [web setDelegate:self];
    [self.view addSubview:web];
    self.socialWeb = web;
    UIActivityIndicatorView *actInd=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    actInd.color=[UIColor blackColor];
    
    [actInd setCenter:self.view.center];
    self.activityIndicator=actInd;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation))
        {
            web.frame = CGRectMake(0, 44, 768, 960);
        }
        if (UIInterfaceOrientationIsLandscape(interfaceOrientation))
        {
       //     orientseason = 1;
        //    self.headOutlet.frame= CGRectMake(0, 0, 1024, 44);
            web.frame = CGRectMake(0, 44, 1024, 722);
        }
        
        
    }else{
        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            web.frame = CGRectMake(0, 44, 320, 416);
        }else{
            web.frame = CGRectMake(0, 44, 320, 510);
            
        }
    }
    
    
    //Add the indicator to the webView to make it visible
    [web addSubview:self.activityIndicator];
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator startAnimating];
    

}
- (void)viewDidAppear:(BOOL)animated{
    [self.socialWeb loadRequest:[NSURLRequest requestWithURL:
                                 [NSURL URLWithString:[self url]] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:20]];
}

-(IBAction)click
{
    [self dismissViewControllerAnimated:YES  completion:nil];
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    
  //  self.mainView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
    
}

-(IBAction)slideBack:(id)sender{
    if ([self.procedence isEqualToString:@"0"]) {
        UIViewController *newTopViewController;
        // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MainViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }
    else if ([self.procedence isEqualToString:@"1"]) {
        UIViewController *newTopViewController;
        //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        MasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"MasView"];
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    }else{
    UIViewController *newTopViewController;
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriasView"];
    
  
    NSString * recST  = [NSString stringWithFormat:@"%@", self.categoriaCOunt ];
    
    P1.categoria = recST;
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)exitAction:(id)sender {
    [self dismissViewControllerAnimated:YES  completion:nil];
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.activityIndicator stopAnimating];
    [self.socialWeb setHidden:NO];
}

@end
