//
//  Ingrediente-RecetaView.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-12.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ingrediente_RecetaView : UIViewController<UIScrollViewDelegate>{

    NSArray *receIng;
    UIScrollView *scrollView;
    UINavigationBar *bar;
    UIButton *botonMenu;
    NSTimer *timer;
    
    UILabel *tittleLabel;
    
    NSArray *ingredienteAC;

}

@property (nonatomic, retain) NSArray *receIng;
@property (nonatomic, strong) NSString *ingrediente;
@property (nonatomic, retain)  UIScrollView *scrollView;
@property(strong, nonatomic) IBOutlet UINavigationBar *bar;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
@property (nonatomic, retain)  NSTimer *timer;
@property (nonatomic, strong) NSString *ingreST;
 @property(strong, nonatomic) IBOutlet   UILabel *tittleLabel;
   @property(strong, nonatomic) IBOutlet  NSArray *ingredienteAC;

@end
