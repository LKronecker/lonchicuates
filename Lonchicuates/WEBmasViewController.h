//
//  WEBmasViewController.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-10.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WEBmasViewController : UIViewController<UIWebViewDelegate>{
    UINavigationBar *bar;
    UIButton *botonMenu;
    UIWebView *socialWeb;
    NSTimer *timer;
   // NSString *url;
}
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic)  UIWebView *detailSocialWebOutlet;
@property (retain, nonatomic)  UIWebView *socialWeb;
@property(strong, nonatomic) IBOutlet UINavigationBar *bar;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
@property  (strong,nonatomic) NSString *url;
@property (nonatomic, retain)  NSTimer *timer;
@property (nonatomic, strong) NSString *categoriaCOunt;

@property (nonatomic, strong) NSString *procedence;

-(IBAction)slideBack:(id)sender;
-(IBAction)click;
@end
