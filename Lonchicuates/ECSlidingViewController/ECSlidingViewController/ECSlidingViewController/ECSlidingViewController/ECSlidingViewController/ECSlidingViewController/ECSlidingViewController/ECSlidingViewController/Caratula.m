//
//  Caratula.m
//  Lonchicuates
//
//  Created by Alan MB on 13/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "Caratula.h"
#import "ECSlidingViewController.h"



@interface Caratula ()

@end

@implementation Caratula
@synthesize pageNumber = _pageNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//metodo para controlar el numero de pagina
- (void)setPageNumber:(NSNumber*)number{
    _pageNumber = number;
}

-(IBAction)clickMenu
{
    [self.slidingViewController anchorTopViewTo:ECRight];

}


@end
