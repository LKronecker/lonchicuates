//
//  TodasRecetasViewController.m
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-03.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//
/*
 
  
 */

#import "TodasRecetasViewController.h"
#import "RecetasViewController.h"
#import "ECSlidingViewController.h"
#import "MasViewController.h"
#import "MainViewController.h"
@interface TodasRecetasViewController ()

@end

@implementation TodasRecetasViewController
@synthesize scrollView, scrollView1, scrollView2, scrollView3, bar, botonMenu, ayudaImage, botonMenu2, CUATE3, timer, displayImage, navTittle, recetasCategoria, botonCERRAR;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{

  //  self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 55, 100, 100)];
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.ayudaImage.alpha=0;
    self.botonCERRAR.alpha = 0;
   
    self.CUATE3.alpha = 0;
    
    NSLog(@"%@", self.categoria);
    
    self.bar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];
    
    if ([self.categoria isEqualToString:@"1"]) {
        
        self.bar.tintColor=[UIColor colorWithRed:((float)0/255.0f) green:((float)130/255.0f) blue:((float)168/255.0f) alpha:1];
        
        self.navTittle.text = @"Más fuerte";
        
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png",@"principal-receta-2.png",@"principal-receta-3.png",@"principal-receta-5.png",@"principal-receta-6.png",@"principal-receta-8.png",@"principal-receta-9.png",@"principal-receta-10.png",@"principal-receta-11.png",@"principal-receta-12.png",@"principal-receta-13.png",@"principal-receta-15.png",@"principal-receta-16.png",@"principal-receta-17.png",@"principal-receta-18.png",@"principal-receta-19.png",@"principal-receta-21.png",@"principal-receta-22.png",@"principal-receta-23.png",@"principal-receta-24.png",@"principal-receta-25.png",@"principal-receta-26.png",@"principal-receta-27.png",@"principal-receta-28.png",@"principal-receta-29.png",@"principal-receta-30.png",@"principal-receta-31.png",@"principal-receta-32.png",@"principal-receta-34.png",@"principal-receta-36.png",@"principal-receta-37.png",@"principal-receta-38.png",@"principal-receta-39.png",@"principal-receta-40.png", nil];
        self.recetasCategoria =array;
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-categoria-1-ipad.png"];
            self.CUATE3.image =[UIImage imageNamed:@"Imagen-categoria-1-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                 self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categoría-1-new.png"];
                 self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-1.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                 self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categor°a-1-iphone5.png"];
                 self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-1.png"];
            }
        }

      
         
        
    }
    if ([self.categoria isEqualToString:@"2"]) {
        
        self.bar.tintColor=[UIColor colorWithRed:((float)212/255.0f) green:((float)159/255.0f) blue:((float)25/255.0f) alpha:1];
        self.navTittle.text = @"Más listo";
        
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png",@"principal-receta-3.png",@"principal-receta-4.png",@"principal-receta-6.png",@"principal-receta-8.png",@"principal-receta-9.png",@"principal-receta-10.png",@"principal-receta-11.png",@"principal-receta-12.png",@"principal-receta-13.png",@"principal-receta-14.png",@"principal-receta-15.png",@"principal-receta-16.png",@"principal-receta-17.png",@"principal-receta-18.png",@"principal-receta-19.png",@"principal-receta-20.png",@"principal-receta-21.png",@"principal-receta-22.png",@"principal-receta-23.png",@"principal-receta-25.png",@"principal-receta-26.png",@"principal-receta-28.png",@"principal-receta-29.png",@"principal-receta-30.png",@"principal-receta-31.png",@"principal-receta-34.png",@"principal-receta-35.png",@"principal-receta-36.png",@"principal-receta-38.png",@"principal-receta-39.png",@"principal-receta-40.png", nil];
        self.recetasCategoria =array;
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-categoria-2-ipad.png"];
            self.CUATE3.image =[UIImage imageNamed:@"Imagen-categoria-2-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categoría-2-new.png"];
                 self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-2.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categor°a-2-iphone5.png"];
                 self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-2.png"];
            }
        }

       

        
    }
    if ([self.categoria isEqualToString:@"3"]) {
        
        self.bar.tintColor=[UIColor colorWithRed:((float)126/255.0f) green:((float)18/255.0f) blue:((float)100/255.0f) alpha:1];
        self.navTittle.text = @"Más energia";
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-2.png",@"principal-receta-3.png",@"principal-receta-4.png",@"principal-receta-5.png",@"principal-receta-7.png",@"principal-receta-9.png",@"principal-receta-11.png",@"principal-receta-14.png",@"principal-receta-17.png",@"principal-receta-18.png",@"principal-receta-23.png",@"principal-receta-24.png",@"principal-receta-26.png",@"principal-receta-29.png",@"principal-receta-30.png",@"principal-receta-31.png",@"principal-receta-32.png",@"principal-receta-33.png",@"principal-receta-34.png",@"principal-receta-35.png",@"principal-receta-36.png",@"principal-receta-40.png",nil];
        self.recetasCategoria =array;
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-categoria-3-ipad.png"];
            self.CUATE3.image =[UIImage imageNamed:@"Imagen-categoria-3-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categoría-3-new.png"];
                self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-3.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categor°a-3-iphone5.png"];
                self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-3.png"];
            }
        }

        
    }
    if ([self.categoria isEqualToString:@"4"]) {
        
        self.bar.tintColor=[UIColor colorWithRed:((float)162/255.0f) green:((float)23/255.0f) blue:((float)47/255.0f) alpha:1];
        self.navTittle.text = @"Más sano";
        
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png",@"principal-receta-2.png",@"principal-receta-3.png",@"principal-receta-4.png",@"principal-receta-5.png",@"principal-receta-6.png",@"principal-receta-7.png",@"principal-receta-8.png",@"principal-receta-9.png",@"principal-receta-10.png",@"principal-receta-11.png",@"principal-receta-12.png",@"principal-receta-13.png",@"principal-receta-14.png",@"principal-receta-15.png",@"principal-receta-16.png",@"principal-receta-17.png",@"principal-receta-18.png",@"principal-receta-19.png",@"principal-receta-20.png",@"principal-receta-21.png",@"principal-receta-22.png",@"principal-receta-23.png",@"principal-receta-24.png",@"principal-receta-25.png",@"principal-receta-26.png",@"principal-receta-27.png",@"principal-receta-28.png",@"principal-receta-29.png",@"principal-receta-30.png",@"principal-receta-31.png",@"principal-receta-32.png",@"principal-receta-33.png",@"principal-receta-34.png",@"principal-receta-35.png",@"principal-receta-36.png",@"principal-receta-37.png",@"principal-receta-38.png",@"principal-receta-39.png",@"principal-receta-40.png",nil];
        self.recetasCategoria =array;
        
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-categoria-4-ipad.png"];
            self.CUATE3.image =[UIImage imageNamed:@"Imagen-categoria-4-ipad.png"];

        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categoría-4-new.png"];
                 self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-4.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-categor°a-4-iphone5.png"];
                 self.CUATE3.image =[UIImage imageNamed:@"Imagen-categor°a-4.png"];
            }
        }

       


    }
    if ([self.categoria isEqualToString:@"5"]) {
        
       self.bar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];
        self.navTittle.text = @"Recetas";
        
        NSArray *array = [[NSArray alloc]initWithObjects:@"principal-receta-1.png",@"principal-receta-2.png",@"principal-receta-3.png",@"principal-receta-4.png",@"principal-receta-5.png",@"principal-receta-6.png",@"principal-receta-7.png",@"principal-receta-8.png",@"principal-receta-9.png",@"principal-receta-10.png",@"principal-receta-11.png",@"principal-receta-12.png",@"principal-receta-13.png",@"principal-receta-14.png",@"principal-receta-15.png",@"principal-receta-16.png",@"principal-receta-17.png",@"principal-receta-18.png",@"principal-receta-19.png",@"principal-receta-20.png",@"principal-receta-21.png",@"principal-receta-22.png",@"principal-receta-23.png",@"principal-receta-24.png",@"principal-receta-25.png",@"principal-receta-26.png",@"principal-receta-27.png",@"principal-receta-28.png",@"principal-receta-29.png",@"principal-receta-30.png",@"principal-receta-31.png",@"principal-receta-32.png",@"principal-receta-33.png",@"principal-receta-34.png",@"principal-receta-35.png",@"principal-receta-36.png",@"principal-receta-37.png",@"principal-receta-38.png",@"principal-receta-39.png",@"principal-receta-40.png",nil];
        self.recetasCategoria =array;
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            ayudaImage.image = [UIImage imageNamed:@"Ayuda-5-ipad.png"];
        }
        
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                // iPhone 3/4x
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-4-new.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                self.ayudaImage.image =[UIImage imageNamed:@"Ayuda-4-iphone5.png"];
            }
        }

        self.CUATE3.image =[UIImage imageNamed:@"LonchiCU-4-7.png"];
        
    }

   

    
    UIImage *menuImg=[UIImage imageNamed:@"Botón-regresarNew.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [bar pushNavigationItem:navItem animated:NO];
    ///
    UIImage *menuImg2=[UIImage imageNamed:@"Botton-ayuda.png"];
    self.botonMenu2 =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu2.frame=CGRectMake(279, 8,  40, 35);
    [self.botonMenu2 setBackgroundImage:menuImg2 forState:UIControlStateNormal];
    [self.botonMenu2  addTarget:self action:@selector(ayuda) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem2= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu2];
    //  UINavigationItem *navItem2=[[UINavigationItem alloc] init];
    navItem.rightBarButtonItem=menuItem2;
    [ self.navigationController.navigationBar addSubview:botonMenu2];
    /////

    //// Primer Scroll
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
         self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(6, 50, 768, 1024)];
     }
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
      self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(6, 50, 320, 505)];
     }
    
     [self.view addSubview: self.scrollView];
     CGSize scrollViewContentSize = CGSizeMake(320, 600);
     [self.scrollView setContentSize:scrollViewContentSize];
     scrollView.delegate = self;
     
     [self.scrollView setBackgroundColor:[UIColor blackColor]];
     [scrollView setCanCancelContentTouches:NO];
     
     scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
     scrollView.clipsToBounds = YES;
     scrollView.scrollEnabled = YES;
     scrollView.pagingEnabled = NO;
     scrollView.alpha = 1;
     scrollView.backgroundColor = nil;
     
     scrollView.scrollsToTop = YES;
     
     NSUInteger nimages = 1;
     CGFloat cx = 0;
     CGFloat cy = 0;
     for (;nimages<15 ; nimages++) {
         int drawCount = 0;
         for (int k = 0 + 3*(nimages-1); k < 3*(nimages); k ++) {
             
             
             if (k < [self.recetasCategoria count]) {
                 
                 NSString *imageName = [NSString stringWithFormat:@"%@", [self.recetasCategoria objectAtIndex:k]];
     
     
     UIImage *image = [UIImage imageNamed:imageName];
             
                  
     if (image == nil) {
     break;
     }
     
     
    // UIImageView *imageView=(UIImageView *)[self.view viewWithTag:k];
      //       [imageView setImage:image];
     
     UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
             
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-1.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap1:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-2.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap2:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-3.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap3:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-4.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap4:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-5.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap5:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-6.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap6:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-7.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap7:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-8.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap8:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-9.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap9:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-10.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap10:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-11.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap11:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-12.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap12:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-13.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap13:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-14.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap14:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-15.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap15:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-16.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap16:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-17.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap17:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-18.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap18:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-19.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap19:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-20.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap20:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-21.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap21:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-22.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap22:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-23.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap23:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-24.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap24:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-25.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap25:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-26.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap26:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-27.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap27:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-28.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap28:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-29.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap29:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-30.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap30:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-31.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap31:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-32.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap32:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-33.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap33:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-34.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap34:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-35.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap35:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-36.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap36:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-37.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap37:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-38.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap38:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-39.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap39:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }
             if ([imageName isEqualToString:[NSString stringWithFormat:@"principal-receta-40.png"]]) {
                 imageView.userInteractionEnabled = YES;
                 
                 UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(receTap40:)];
                 recognizer.numberOfTapsRequired = 1;
                 recognizer.numberOfTouchesRequired = 1;
                 
                 [imageView addGestureRecognizer:recognizer];
                 
             }





                 
                 if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                     CGRect rect = imageView.frame;
                     rect.size.height = 250;
                     rect.size.width = 250;
                     rect.origin.x = ((scrollView.frame.size.width - (image.size.width))*(drawCount)/2) +cx;
                     
                     rect.origin.y = 250*(nimages-1) + cy;
                     imageView.frame = rect;
                     
                 }


      if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
     CGRect rect = imageView.frame;
     rect.size.height = 100;
     rect.size.width = 100;
     rect.origin.x = ((scrollView.frame.size.width - (image.size.width))*(drawCount)) +cx;
             
     rect.origin.y = 100*(nimages-1) + cy;
     imageView.frame = rect;
          
      }
             
          //   NSLog(@"%i, %f %f", drawCount, cx, imageView.frame.origin.x);
                 
             
     ///////////
             
     [scrollView addSubview:imageView];
             }
     // [imageView release];
             
             drawCount = (drawCount+1)%4;
             
             if (drawCount < 3) {
                 if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                     cx += scrollView.frame.size.width/30;
                 }
                 if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                cx += scrollView.frame.size.width/3.3;
                 }
             } else{
                 cx = 0;
             }
             
            

     
     }
                        
          cy += scrollView.frame.size.height/80;
     
     }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+15*250)];
    }
  
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){

     [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, cy+15*100)];
    }
    
  //  [scrollView setContentSize:CGSizeMake([scrollView bounds].size.width, (cy+[self.recetasCategoria count]/3)*100)];
    
    ////

    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ayudaCERRAR)];
    recognizer.numberOfTapsRequired = 1;
   recognizer.numberOfTouchesRequired = 1;
   self.ayudaImage.userInteractionEnabled = YES;
  [self.ayudaImage addGestureRecognizer:recognizer];
    
 // [self.view bringSubviewToFront:recetaPicker];
}





-(IBAction)click
{
    [self dismissViewControllerAnimated:YES  completion:nil];
    //[self.slidingViewController anchorTopViewTo:ECRight];
    
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    
    //  self.mainView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
    
}

-(IBAction)slideBack:(id)sender{
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UIViewController *newTopViewController;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
        
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];

    
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            
            UIViewController *newTopViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
        }
        else if (iOSDeviceScreenSize.height == 568){
            
            UIViewController *newTopViewController;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard_iPhone5" bundle:nil];
            MainViewController *P1 = [storyboard instantiateViewControllerWithIdentifier:@"Home"];
            
            newTopViewController = P1;
            
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController resetTopView];
            
        }
    }
}

-(IBAction)ayuda{
   
    self.botonCERRAR.alpha = 1;
     self.ayudaImage.alpha = 1;
   
       //self.scrollView.alpha = 0;
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.CUATE3.alpha = 1;
    [UIView commitAnimations];
    
    [self.view bringSubviewToFront:ayudaImage];
    [self.view bringSubviewToFront:CUATE3];
     [self.view bringSubviewToFront:botonCERRAR];
   
    self.navigationController.navigationBarHidden = YES;
    //   self.opcionesView.userInteractionEnabled = NO;
    //  self.recetaView.userInteractionEnabled = NO;
    NSLog(@"ayuda");
}
-(IBAction)ayuda2{
    
    
  //  self.scrollView.alpha = 1;
    
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.ayudaImage.alpha = 0;
    
    self.CUATE3.alpha = 0;
    [UIView commitAnimations];
    
    
    //   self.opcionesView.userInteractionEnabled = YES;
    //  self.recetaView.userInteractionEnabled = YES;
    NSLog(@"ayuda");
    self.navigationController.navigationBarHidden = NO;
    [ self.navigationController.navigationBar addSubview:botonMenu];
    [ self.navigationController.navigationBar addSubview:botonMenu2];
}
-(IBAction)ayudaCERRAR{
    
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.ayudaImage.alpha = 0;
    self.botonCERRAR.alpha = 0;
      self.CUATE3.alpha = 0;
    [UIView commitAnimations];
    
    
    self.navigationController.navigationBarHidden = NO;
    [ self.navigationController.navigationBar addSubview:botonMenu];
    [ self.navigationController.navigationBar addSubview:botonMenu2];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)exitAction:(id)sender {
    [self dismissViewControllerAnimated:YES  completion:nil];
}

-(IBAction)receTap1:(id)sender{
            
        UIViewController *newTopViewController;
       // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
        RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
        
        NSString * recST  = [NSString stringWithFormat:@"1"];
    
    P1.categoST = self.categoria;
        P1.receta = recST;
        P1.procedence = @"1";
        newTopViewController = P1;
        
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];

    }
-(IBAction)receTap2:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"2"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap3:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"3"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap4:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"4"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap5:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"5"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap6:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"6"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];

    }
-(IBAction)receTap7:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"7"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap8:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"8"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap9:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"9"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];

}
-(IBAction)receTap10:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"10"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap11:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"11"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap12:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"12"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap13:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"13"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap14:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"14"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap15:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"15"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap16:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"16"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap17:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"17"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap18:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"18"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap19:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"19"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap20:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"20"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap21:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"21"];
     P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap22:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"22"];
      P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap23:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"23"];
      P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap24:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"24"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap25:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"25"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap26:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"26"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap27:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"27"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap28:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"28"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap29:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"29"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap30:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"30"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap31:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"31"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap32:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"32"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];

}
-(IBAction)receTap33:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"33"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap34:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"34"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap35:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"35"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap36:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"36"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap37:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"37"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap38:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"38"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap39:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"39"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}
-(IBAction)receTap40:(id)sender{
    UIViewController *newTopViewController;
    // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"40"];
    P1.categoST = self.categoria;
    P1.receta = recST;
    P1.procedence = @"1";
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
}









@end
