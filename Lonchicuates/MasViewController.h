//
//  MasViewController.h
//  Lonchicuates
//
//  Created by Leopoldo G Vargas on 2013-09-10.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 

@interface MasViewController : UIViewController <UIWebViewDelegate, UIScrollViewDelegate, MFMailComposeViewControllerDelegate>{
    UINavigationBar *bar;
     UIButton *botonMenu;

    NSTimer *timer;
    
    UIView *mainView;
    UIWebView *webView;
    UIScrollView *scrollView;
    NSTimer *timerSlide;
    
    int categoriaCount;
    int socialBool;
    UIScrollView *scrollViewAv;
    NSString *avisoST;
   // UIActivityIndicator *activityIndicator;
    
     UIImageView * CUATE3;
     UIButton *botonMenu2;
    UIImageView * ayudaImage;
    UIButton *botonCERRAR;
    bool avisoBOOL;
}
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicator;

@property(strong, nonatomic) IBOutlet UINavigationBar *bar;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu;
@property (nonatomic, retain)  NSTimer *timer;
  @property (nonatomic, retain) IBOutlet  UIView *mainView;
 @property (nonatomic, retain)IBOutlet UIWebView *webView;
@property (nonatomic, retain)  UIScrollView *scrollView;
@property (nonatomic, retain)  UIScrollView *scrollViewAv;
@property (nonatomic, retain)  NSTimer *timerSlide;
@property (nonatomic, retain) NSString *avisoST;

@property(nonatomic,strong)IBOutlet UIButton *botonCERRAR;


@property(strong, nonatomic) IBOutlet UIImageView * CUATE3;
@property(nonatomic,strong)IBOutlet UIButton *botonMenu2;
@property(nonatomic,strong)IBOutlet  UIImageView * ayudaImage;

-(IBAction)click;
-(IBAction)slideBack:(id)sender;
-(IBAction)loadWeb;
-(IBAction)aviso;
-(IBAction)AvisoBack;
-(IBAction)FaceBook;
-(IBAction)Twitter;
-(IBAction)YouTube;
-(IBAction)contactanos;
-(IBAction)ayuda;
-(IBAction)ayuda2;
-(IBAction)ayudaCERRAR;
-(IBAction)MASayuda;
@end
