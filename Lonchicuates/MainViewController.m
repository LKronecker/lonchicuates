//
//  MainViewController.m
//  Lonchicuates
//
//  Created by Alan MB on 12/08/13.
//  Copyright (c) 2013 Alan MB. All rights reserved.
//

#import "MainViewController.h"
#import "ECSlidingViewController.h"
#import "MenuViewController.h"

#import "TodasRecetasViewController.h"

#import "DetailSocialViewController.h"
#import "RecetasViewController.h"
#import "CategoriasViewController.h"
#import "WEBmasViewController.h"
#import "MasViewController.h"
#import "AppDelegate.h"

@interface MainViewController ()

@property (nonatomic, retain)IBOutlet UIView *opcionesView;

@end

@implementation MainViewController
@synthesize recetaPicker, recetaView, timer, recetaButon, recetasAray, timerSlide, bar, botonMenu, ayudaImage, botonMenu2, recetasINDEX, CUATE1, CUATE2, CUATE3, scrollView, botonCERRAR, flechaImage, flechaTimer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

-(void)viewDidDisappear:(BOOL)animated{

    [timer invalidate];
    timer = nil;
    
     slideControl = 0;
   // self.recetaButon.alpha = 0;
}
-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:@"firstRun"])
        [defaults setObject:[NSDate date] forKey:@"firstRun"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];


}

- (void)viewDidAppear:(BOOL)animated{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.3];
    
    
    self.scrollView.alpha = 1;
  //  self.recetaPicker.alpha = 1;
    self.recetaButon.alpha = 1;
    
    
    [UIView commitAnimations];
       timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(showTime:) userInfo:nil repeats:YES];
    
    //////////
    
    slideControl = 0;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"firstRun"])
    {
       // [self ayuda];
    }

   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
  //   self.view.autoresizesSubviews = NO;
   // self.scrollView.autoresizesSubviews = NO;
    
        
    self.botonCERRAR.alpha = 0;
    
        self.ayudaImage.alpha = 0;
    self.CUATE1.alpha = 0;
    self.CUATE2.alpha = 0;
    self.CUATE3.alpha = 0;
     //   self.recetaButon.alpha = 0;
    
    recetaCOunt = 0;
    
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        NSArray *recetasAr = [[NSArray alloc]initWithObjects:@"Imagen-principal-receta-11-ipad.png",@"Imagen-principal-receta-17-ipad.png",@"Imagen-principal-receta-22-ipad.png",@"Imagen-principal-receta-26-ipad.png",@"Imagen-principal-receta-36-ipad.png", nil];
        self.recetasAray =recetasAr;

    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        NSArray *recetasAr = [[NSArray alloc]initWithObjects:@"Imagen-principal-receta-11",@"Imagen-principal-receta-17",@"Imagen-principal-receta-22",@"Imagen-principal-receta-26",@"Imagen-principal-receta-36", nil];
        self.recetasAray =recetasAr;

    }

       
    NSArray *receInd = [[NSArray alloc]initWithObjects:@"11",@"17",@"22",@"26",@"36", nil];
    self.recetasINDEX = receInd;
  

    /////Imagen-principal-receta-11-ipad
    recetaPicker.animationImages = [NSArray arrayWithObjects:
                                    [UIImage imageNamed:@"Imagen-principal-receta-11.png"],
                                    [UIImage imageNamed:@"Imagen-principal-receta-17.png"],
                                    [UIImage imageNamed:@"Imagen-principal-receta-22.png"],
                                    [UIImage imageNamed:@"Imagen-principal-receta-26.png"],
                                    [UIImage imageNamed:@"Imagen-principal-receta-36.png"],
                                    nil];
	//int duration = [slabel.text intValue];
	//[carSlideShow setAnimationRepeatCount:duration];
	
    recetaPicker.animationDuration = [self.recetasAray count]*4;
	[recetaPicker startAnimating];
    
   
///////R 209 G241 B113
    
   self.navigationController.navigationBar.tintColor=[UIColor colorWithRed:((float)194/255.0f) green:((float)224/255.0f) blue:((float)103/255.0f) alpha:1];
    
	// Do any additional setup after loading the view.
    UIImage *menuImg=[UIImage imageNamed:@"Bot¢n-header.png"];
    self.botonMenu =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu.frame=CGRectMake(5, 10, 29, 24);
    [self.botonMenu setBackgroundImage:menuImg forState:UIControlStateNormal];
    [self.botonMenu  addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu];
    UINavigationItem *navItem=[[UINavigationItem alloc] init];
    navItem.leftBarButtonItem=menuItem;
    [ self.navigationController.navigationBar pushNavigationItem:navItem animated:NO];
    ////
    UIImage *menuImg2=[UIImage imageNamed:@"Botton-ayuda.png"];
    self.botonMenu2 =[UIButton buttonWithType:UIButtonTypeCustom];
    self.botonMenu2.frame=CGRectMake(278, 1, 40, 35);
    [self.botonMenu2 setBackgroundImage:menuImg2 forState:UIControlStateNormal];
    [self.botonMenu2  addTarget:self action:@selector(ayuda) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *menuItem2= [[UIBarButtonItem alloc ]initWithCustomView:self.botonMenu2];
  //  UINavigationItem *navItem2=[[UINavigationItem alloc] init];
    navItem.rightBarButtonItem=menuItem2;
    [ self.navigationController.navigationBar addSubview:botonMenu2];
    /////
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        UIGraphicsBeginImageContext (CGSizeMake(768, 44));
        [[UIImage imageNamed:@"Header-ipad.png"] drawInRect:(CGRectMake(0, 0, 768, 44))];
        UIImage *imageHead = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        [self.navigationController.navigationBar setBackgroundImage:imageHead forBarMetrics:UIBarMetricsDefault];


}
  else  if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
    UIGraphicsBeginImageContext (CGSizeMake(320, 44));
    [[UIImage imageNamed:@"Header.png"] drawInRect:(CGRectMake(0, 0, 320, 44))];
    UIImage *imageHead = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    [self.navigationController.navigationBar setBackgroundImage:imageHead forBarMetrics:UIBarMetricsDefault];
    
   // [self.view bringSubviewToFront:self.navigationController.navigationBar];
    
    }
       //////
    
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 10.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    ///////
    
    if (![self.slidingViewController.underLeftViewController isKindOfClass:[MenuViewController class]]) {
        self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    
  //  [self.navigationController.navigationBar addGestureRecognizer:self.slidingViewController.panGesture];
   // [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    //////
    
    //UISwipeGestureRecognizer *SwipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(inViewSwipe:)];
    
    //[SwipeUp setDirection:(UISwipeGestureRecognizerDirectionUp| UISwipeGestureRecognizerDirectionDown)];
    
   // [[self opcionesView] addGestureRecognizer:SwipeUp];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ayuda2)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    self.ayudaImage.userInteractionEnabled = YES;
    [self.ayudaImage addGestureRecognizer:recognizer];
    
   // [self.view bringSubviewToFront:recetaPicker];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)];
            
        }
        
        else if (iOSDeviceScreenSize.height == 568){
            self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 548)];
        }
    }
     if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
    
    [self.view addSubview: self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake(320, 600);
    [self.scrollView setContentSize:scrollViewContentSize];
    scrollView.delegate = self;
    
    [self.scrollView setBackgroundColor:[UIColor blackColor]];
    [scrollView setCanCancelContentTouches:NO];
    
    scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    scrollView.clipsToBounds = YES;
    scrollView.scrollEnabled = YES;
    scrollView.pagingEnabled = NO;
    scrollView.alpha = 1;
    scrollView.backgroundColor = nil;
    
    scrollView.scrollsToTop = YES;
    ////
    
    UIButton *receButt = [UIButton buttonWithType:UIButtonTypeCustom];
    
    receButt.frame = CGRectMake(7, 6, 305, 244);
    [receButt setImage:[UIImage imageNamed: @"Imagen-principal-receta-11.png"] forState:UIControlStateNormal];
    
    [receButt addTarget:self action:@selector(pushReceta1) forControlEvents:UIControlEventTouchUpInside];
    
    [self.scrollView addSubview:receButt];
    self.recetaButon = receButt;
    ////
    
    UIImageView *imageREceta= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Botón recetas.png"]]];
    imageREceta.frame = CGRectMake(6, 258, 308, 130);
    
    
    imageREceta.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogReceta = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TodasRecetas)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageREceta addGestureRecognizer:recogReceta];
    
    [scrollView addSubview:imageREceta];
    //////

    
    
    ////
    UIImageView *imageMFUerte= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Categor°a-1.png"]]];
    imageMFUerte.frame = CGRectMake(6, 396, 150, 130);
    
    
    imageMFUerte.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasfuerte = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushMasFuerte)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMFUerte addGestureRecognizer:recogMasfuerte];
    
    [scrollView addSubview:imageMFUerte];
    //////
    UIImageView *imageMListo= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Categor°a-2.png"]]];
    imageMListo.frame = CGRectMake(164, 396, 150, 130);
    
    
    imageMListo.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasLis = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushMasListo)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMListo addGestureRecognizer:recogMasLis];
    
    [scrollView addSubview:imageMListo];
    /////

    UIImageView *imageMEnergia= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Categor°a-3.png"]]];
    imageMEnergia.frame = CGRectMake(6, 534, 150, 130);
    
    
    imageMEnergia.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasEner = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushMasEnergia)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMEnergia addGestureRecognizer:recogMasEner];
    
    [scrollView addSubview:imageMEnergia];
    ///
    /////
    UIImageView *imageMLiSan= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Categor°a-4.png"]]];
    imageMLiSan.frame = CGRectMake(164, 534, 150, 130);
    
    
    imageMLiSan.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogMasSan = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushMasSano)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMLiSan addGestureRecognizer:recogMasSan];
    
    [scrollView addSubview:imageMLiSan];
    /////
    
    UIImageView *imageCuates= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Categor°a-5.png"]]];
    imageCuates.frame = CGRectMake(6   , 672, 150, 130);
    
    
    imageCuates.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogCUa = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Lonchicuates)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageCuates addGestureRecognizer:recogCUa];
    
    [scrollView addSubview:imageCuates];
    /////
    UIImageView *imageAtletas= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Categor°a-6.png"]]];
    imageAtletas.frame = CGRectMake(164  , 672, 150, 130);
    
    
    imageAtletas.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogATL = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Lonchiatletas)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageAtletas addGestureRecognizer:recogATL];
    
    [scrollView addSubview:imageAtletas];
    /////
    
    UIImageView *imageFB= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Facebook.png"]]];
    imageFB.frame = CGRectMake(6  , 810, 150, 130);
    
    
    imageFB.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogFB = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(FaceBook)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageFB addGestureRecognizer:recogFB];
    
    [scrollView addSubview:imageFB];
    /////
    
    UIImageView *imageTW= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"Twitter.png"]]];
    imageTW.frame = CGRectMake(164  , 810, 150, 130);
    
    
    imageTW.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *recogTW = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Twitter)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageTW addGestureRecognizer:recogTW];
    
    [scrollView addSubview:imageTW];
    /////
    UIImageView *imageYou= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MainViewYoutube.png"]]];
    imageYou.frame = CGRectMake(6  , 948, 150, 130);
    
    
    imageYou.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapyou = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(YouTube)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageYou addGestureRecognizer:tapyou];
    
    [scrollView addSubview:imageYou];
    ////
    UIImageView *imageMas= [[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"MainViewMás.png"]]];
    imageMas.frame = CGRectMake(164  , 948, 150, 130);
    
    
    imageMas.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapMAS = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(Mas)];
    recognizer.numberOfTapsRequired = 1;
    recognizer.numberOfTouchesRequired = 1;
    
    [imageMas addGestureRecognizer:tapMAS];
    
    [scrollView addSubview:imageMas];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            [scrollView setContentSize:CGSizeMake(320, 1093)];
            
        }
        
        else if (iOSDeviceScreenSize.height == 568){
            [scrollView setContentSize:CGSizeMake(320, 1150)];
        }
    }
}
    
 ayudaCount = 0;
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
    
    self.flechaImage.alpha = 1;
    [self.view bringSubviewToFront:self.flechaImage];
    
    flechaTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(EndAnimaconFlecha:) userInfo:nil repeats:NO];
    }
}

- (IBAction)BegginAnimaconFlecha:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.flechaImage.alpha = 1;
    
    [UIView commitAnimations];
    flechaTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(EndAnimaconFlecha:) userInfo:nil repeats:NO];
    
    
}
- (IBAction)EndAnimaconFlecha:(id)sender{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    self.flechaImage.alpha = 0;
    
    [UIView commitAnimations];
    
    flechaTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(BegginAnimaconFlecha:) userInfo:nil repeats:NO];
    
    
}

-(IBAction)ayuda{
    
    self.ayudaImage.alpha = 1;
    self.botonCERRAR.alpha = 1;
    ayudaCount = 1;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.7];
    
    if (slideControl == 1) {

    CGRect scrlFrame = self.scrollView.frame;
    scrlFrame.origin.y +=398;
    self.scrollView.frame = scrlFrame;
    }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        ayudaImage.image = [UIImage imageNamed:@"Ayuda-1-ipad.png"];
    }
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
             ayudaImage.image = [UIImage imageNamed:@"Ayuda-1-new.png"];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
             ayudaImage.image = [UIImage imageNamed:@"Ayuda-1-iphone5-new.png"];
        }
    }
    
    self.CUATE1.alpha = 1;
   // self.CUATE2.alpha = 1;
   // self.CUATE3.alpha = 1;
    [UIView commitAnimations];

    [self.view bringSubviewToFront:ayudaImage];
    
   // [self.view bringSubviewToFront:CUATE2];
    [self.view bringSubviewToFront:CUATE1];
    
    [self.view bringSubviewToFront:botonCERRAR];
    self.navigationController.navigationBarHidden = YES;
        NSLog(@"ayuda");
}
-(IBAction)ayuda2{
    
    if (ayudaCount ==0) {
        ayudaCount = 1;
        self.botonCERRAR.alpha = 1;
        
        self.ayudaImage.alpha = 0;
        slideControl = 0;
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
          ayudaImage.image = [UIImage imageNamed:@"Ayuda-1-ipad.png"];
        }
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
            // The iOS device = iPhone or iPod Touch
            CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
            if (iOSDeviceScreenSize.height == 480){
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-1-new.png"];
                
            }else if (iOSDeviceScreenSize.height == 568){
                // iPhone 5 etc
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-1-iphone5-new.png"];
            }
        }

         self.ayudaImage.alpha = 1;
        self.CUATE1.alpha = 1;
        // self.CUATE2.alpha = 1;
        // self.CUATE3.alpha = 1;
        [UIView commitAnimations];
        
        [self.view bringSubviewToFront:ayudaImage];
        // 
         [self.view bringSubviewToFront:CUATE1];
        [self.view bringSubviewToFront:botonCERRAR];
       
        self.navigationController.navigationBarHidden = YES;
          
    }

  else  if (ayudaCount ==1) {
        ayudaCount = 2;
        
     self.ayudaImage.alpha = 0;
    slideControl = 0;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
      
      if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
          ayudaImage.image = [UIImage imageNamed:@"Ayuda-2-ipad.png"];
      }
   
      if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
          // The iOS device = iPhone or iPod Touch
          CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
          if (iOSDeviceScreenSize.height == 480){
              // iPhone 3/4x
              ayudaImage.image = [UIImage imageNamed:@"Ayuda-2-new.png"];
              
          }else if (iOSDeviceScreenSize.height == 568){
              // iPhone 5 etc
              ayudaImage.image = [UIImage imageNamed:@"Ayuda-2-iphone5-new.png"];
          }
      }

    self.CUATE1.alpha = 0;
    self.CUATE2.alpha = 1;
   // self.CUATE3.alpha = 0;
    self.ayudaImage.alpha = 1;
    [UIView commitAnimations];
        
         [self.view bringSubviewToFront:CUATE2];
    }
        else if (ayudaCount ==2){
            ayudaCount = 0;
            self.ayudaImage.alpha = 0;
            [UIView beginAnimations:@"advancedAnimations" context:nil];
            [UIView setAnimationDuration:0.4];
            
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
                ayudaImage.image = [UIImage imageNamed:@"Ayuda-3-ipad.png"];
            }
            
            if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
                // The iOS device = iPhone or iPod Touch
                CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
                if (iOSDeviceScreenSize.height == 480){
                    // iPhone 3/4x
                    ayudaImage.image = [UIImage imageNamed:@"Ayuda-3-new.png"];
                    
                }else if (iOSDeviceScreenSize.height == 568){
                    // iPhone 5 etc
                    ayudaImage.image = [UIImage imageNamed:@"Ayuda-3-iphone5-new.png"];
                }
            }

            self.CUATE1.alpha = 0;
            self.CUATE2.alpha = 0;
             self.CUATE3.alpha = 1;
            self.ayudaImage.alpha = 1;
            [UIView commitAnimations];

             [self.view bringSubviewToFront:CUATE3];
        
        

    }
        }

-(IBAction)ayudaCERRAR{
    self.ayudaImage.alpha = 0;
    slideControl = 0;
    self.botonCERRAR.alpha = 0;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    
    self.CUATE1.alpha = 0;
    self.CUATE2.alpha = 0;
    self.CUATE3.alpha = 0;
    [UIView commitAnimations];
    
    
    self.opcionesView.userInteractionEnabled = YES;
    self.recetaView.userInteractionEnabled = YES;
    
    self.navigationController.navigationBarHidden = NO;
    [ self.navigationController.navigationBar addSubview:botonMenu];
    [ self.navigationController.navigationBar addSubview:botonMenu2];
}

-(IBAction)showTime:(id)sender{
    if (recetaCOunt < [self.recetasAray count]-1) {
        
    recetaCOunt = recetaCOunt + 1;
               
                   NSString *rece = [NSString stringWithFormat:@"%@", [self.recetasAray objectAtIndex:recetaCOunt]];

        [self.recetaButon setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@", rece]] forState:UIControlStateNormal];
        
    }else{
        recetaCOunt = 0;
                   NSString *rece = [NSString stringWithFormat:@"%@", [self.recetasAray objectAtIndex:recetaCOunt]];
            
            [self.recetaButon setImage:[UIImage imageNamed:[NSString stringWithFormat: @"%@", rece]] forState:UIControlStateNormal];
        }

   
}

- (IBAction)tapReceta:(id)sender{
    UIViewController *newTopViewController;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"RecetasMain"];
    NSString * recST  = [NSString stringWithFormat:@"%@", [self.recetasINDEX objectAtIndex:recetaCOunt]];
    P1.receta = recST;
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    [self.slidingViewController resetTopView];
    self.slidingViewController.topViewController =newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
  

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//acciones implementada sobre la vista
-(IBAction)click
{
    [self.slidingViewController anchorTopViewTo:ECRight];
    
  /*  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
        
        [UIView commitAnimations];
        
        
    }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
   */
}

-(IBAction)pushReceta1
{
       
   // [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
    self.scrollView.alpha = 0;
    
    self.recetaButon.alpha = 0;
        
        [UIView commitAnimations];

       
   // }];
      if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
     
     timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack:) userInfo:nil repeats:NO];
      }
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        
        timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(tapReceta:) userInfo:nil repeats:NO];
    }
    
}

-(IBAction)slideBack:(id)sender{
       UIViewController *newTopViewController;
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    RecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"RecetasMain"];
    
    NSString * recST  = [NSString stringWithFormat:@"%@", [self.recetasINDEX objectAtIndex:recetaCOunt]];
    
    P1.receta = recST;
       newTopViewController = P1;
          
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
            
          }
 

-(IBAction)pushMail{
    NSString *url = [NSString stringWithFormat: @"mailto:contacto@lonchicuates.com?&subject=&body="];
    [[UIApplication sharedApplication] openURL: [NSURL URLWithString: url]];
    
    
}
-(IBAction)TodasRecetas{
     UIViewController *newTopViewController;
    //UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"TodasRecetas"];
    
    NSString * catST  = [NSString stringWithFormat:@"5"];
    
    P1.categoria = catST;
    //  P1.categoria= @"5";
    // self.recetas = rec;
    
    //  NSLog(@"%@", recST);
    
    newTopViewController = P1;
    
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];

}
-(IBAction)pushMasFuerte
{
    categoriaCount = 1;
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
        
        [UIView commitAnimations];
        
        
  //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];}

-(IBAction)pushMasListo{
    categoriaCount = 2;
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
        self.scrollView.alpha =0;
        [UIView commitAnimations];
        
        
  //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}
-(IBAction)pushMasEnergia{
    categoriaCount = 3;
   // [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
        
        [UIView commitAnimations];
        
        
   // }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}
-(IBAction)pushMasSano{
    categoriaCount = 4;
   // [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
        
        [UIView commitAnimations];
        
        
  //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}
-(IBAction)Lonchicuates{
    categoriaCount = 5;
   // [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
        
        [UIView commitAnimations];
        
        
  //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}
-(IBAction)Lonchiatletas{
    categoriaCount = 6;
  //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.4];
        self.opcionesView.alpha = 0;
        self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
        
        [UIView commitAnimations];
        
        
  //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}

-(IBAction)FaceBook{
    categoriaCount = 7;
    socialBool = 0;
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.opcionesView.alpha = 0;
    self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}

-(IBAction)Twitter{
    categoriaCount = 7;
    socialBool = 1;
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.opcionesView.alpha = 0;
    self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}

-(IBAction)YouTube{
    categoriaCount = 7;
    socialBool = 2;
    //  [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.opcionesView.alpha = 0;
    self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}
-(IBAction)Mas{
    
    categoriaCount = 8;
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.4];
    self.opcionesView.alpha = 0;
    self.recetaView.alpha = 0;
    self.scrollView.alpha =0;
    
    [UIView commitAnimations];
    
    
    //  }];
    
    timerSlide = [NSTimer scheduledTimerWithTimeInterval:.4 target:self selector:@selector(slideBack2:) userInfo:nil repeats:NO];
}


-(IBAction)slideBack2:(id)sender{
    if (categoriaCount ==5) {
       
    
    UIViewController *newTopViewController;
  //  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriasView"];
  
        NSString * recST  = [NSString stringWithFormat:@"%i", categoriaCount];
        
        P1.categoria = recST;
  
    
    
    
    newTopViewController = P1;
           CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
       
    }
   else if (categoriaCount ==6) {
        
        
        UIViewController *newTopViewController;
    //    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
        CategoriasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoriasView"];
  
        NSString * recST  = [NSString stringWithFormat:@"%i", categoriaCount];
        
        P1.categoria = recST;
        newTopViewController = P1;
   
       CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
        
    }
    
   else if (categoriaCount ==7) {
       
       
       UIViewController *newTopViewController;
      // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
       WEBmasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier: @"WEBmas"];
       
        P1.procedence = @"0";
       if (socialBool == 0) {
       NSString * catST  = [NSString stringWithFormat:@"https://www.facebook.com/Lonchicuates"];
           P1.url = catST;
       }
       
       if (socialBool == 1) {
           NSString * catST  = [NSString stringWithFormat:@"https://www.twitter.com/Lonchicuates"];
           P1.url = catST;
       }
       if (socialBool == 2) {
           NSString * catST  = [NSString stringWithFormat:@"https://www.youtube.com/user/Lonchicuates"];
           P1.url = catST;
       }


            newTopViewController = P1;
       
       CGRect frame = self.slidingViewController.topViewController.view.frame;
       self.slidingViewController.topViewController = newTopViewController;
       self.slidingViewController.topViewController.view.frame = frame;
       [self.slidingViewController resetTopView];
       
   }
    
   else if (categoriaCount ==8) {
   
           UIViewController *newTopViewController;
        //   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
           MasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"MasView"];
           
           //  NSString * recST  = [NSString stringWithFormat:@"%i", categoriaCount];
           
           //P1.categoria = recST;
           newTopViewController = P1;
           
           CGRect frame = self.slidingViewController.topViewController.view.frame;
           self.slidingViewController.topViewController = newTopViewController;
           self.slidingViewController.topViewController.view.frame = frame;
           [self.slidingViewController resetTopView];
     //  }
   }


    
   else{
       UIViewController *newTopViewController;
      // UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
       TodasRecetasViewController *P1 = [self.storyboard instantiateViewControllerWithIdentifier:@"TodasRecetas"];
    
       NSString * recST  = [NSString stringWithFormat:@"%i", categoriaCount];
       
       P1.categoria = recST;
       newTopViewController = P1;
       
       
       
       // [self.slidingViewController resetTopViewWithAnimations:nil onComplete:^{
       
       CGRect frame = self.slidingViewController.topViewController.view.frame;
       self.slidingViewController.topViewController = newTopViewController;
       self.slidingViewController.topViewController.view.frame = frame;
       [self.slidingViewController resetTopView];
   
   }

  //}];
    
    
    //  [self.slidingViewController resetTopView];
}


-(IBAction)inViewSwipe:(id)sender{
    
    if (slideControl == 0) {
        
        scrollView.scrollEnabled = YES;
    
    // [self.detailView removeFromSuperview];
    [self.slidingViewController  resetStrategy];
    
    [UIView beginAnimations:@"advancedAnimations" context:nil];
    [UIView setAnimationDuration:0.6];
    
   
        CGRect viewFrame = self.opcionesView.frame;
        CGRect scrlFrame = self.scrollView.frame;
        CGRect recetFrame = self.recetaView.frame;
        //  CGRect tableFrame = indcatorsTable.frame;
        
        scrlFrame.origin.y -=398;
        viewFrame.origin.y -= 398;
        recetFrame.origin.y -= 398;
        //   tableFrame.origin.x += 270;
        
        self.opcionesView.frame = viewFrame;
        self.recetaView.frame =recetFrame;
        self.scrollView.frame = scrlFrame;
        //  indcatorsTable.frame = tableFrame;
    
      //  CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
       // [self.scrollView setContentOffset:bottomOffset animated:YES];
        
    [UIView commitAnimations];
    
  //  [self.view removeGestureRecognizer:self.slidingViewController.panGesture];
        
        slideControl = 1;
        
        
    
        
    }else if (slideControl == 1){
    
    
        [self.slidingViewController  resetStrategy];
        scrollView.scrollEnabled = NO;
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        
        CGRect viewFrame = self.opcionesView.frame;
        CGRect scrlFrame = self.scrollView.frame;
        CGRect recetFrame = self.recetaView.frame;
        //  CGRect tableFrame = indcatorsTable.frame;
        
        scrlFrame.origin.y +=398;
        viewFrame.origin.y += 398;
        recetFrame.origin.y += 398;
        //   tableFrame.origin.x += 270;
        
        self.opcionesView.frame = viewFrame;
        self.recetaView.frame =recetFrame;
        self.scrollView.frame = scrlFrame;

        
        
        
        [UIView commitAnimations];
        
      //  [self.view addGestureRecognizer:self.slidingViewController.panGesture];
        
     //   [self.recetaPicker reloadInputViews];
        
        

    slideControl = 0;
    
    }
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    slideControl = 0;
    NSLog(@"heeey");
    if ([segue.identifier isEqualToString:@"goToFB"]) {
        DetailSocialViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.facebook.com/Lonchicuates";
        
        
    }
    else if ([segue.identifier isEqualToString:@"goToTwittwer"]) {
        DetailSocialViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.twitter.com/Lonchicuates";
               

        
    }else if ([segue.identifier isEqualToString:@"goToYoutube"]) {
        DetailSocialViewController *destViewController = segue.destinationViewController;
        destViewController.url=@"https://www.youtube.com/user/Lonchicuates";
        
        
    }

 
    
}


@end
